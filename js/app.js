"use strict";

/**
 * Подключение JS файлов которые начинаются с подчеркивания
 */

/**
 * Возвращает функцию, которая не будет срабатывать, пока продолжает вызываться.
 * Она сработает только один раз через N миллисекунд после последнего вызова.
 * Если ей передан аргумент `immediate`, то она будет вызвана один раз сразу после
 * первого запуска.
 */
function debounce(func, wait, immediate) {
  var timeout = null,
      context = null,
      args = null,
      later = null,
      callNow = null;
  return function () {
    context = this;
    args = arguments;

    later = function later() {
      timeout = null;

      if (!immediate) {
        func.apply(context, args);
      }
    };

    callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);

    if (callNow) {
      func.apply(context, args);
    }
  };
} // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
// MIT license


;

(function () {
  var lastTime = 0,
      vendors = ['ms', 'moz', 'webkit', 'o'],
      x,
      currTime,
      timeToCall,
      id;

  for (x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
    window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
  }

  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function (callback) {
      currTime = new Date().getTime();
      timeToCall = Math.max(0, 16 - (currTime - lastTime));
      id = window.setTimeout(function () {
        callback(currTime + timeToCall);
      }, timeToCall);
      lastTime = currTime + timeToCall;
      return id;
    };
  }

  if (!window.cancelAnimationFrame) {
    window.cancelAnimationFrame = function (id) {
      clearTimeout(id);
    };
  }
})();

;

(function () {
  // Test via a getter in the options object to see if the passive property is accessed
  var supportsPassiveOpts = null;

  try {
    supportsPassiveOpts = Object.defineProperty({}, 'passive', {
      get: function get() {
        window.supportsPassive = true;
      }
    });
    window.addEventListener('est', null, supportsPassiveOpts);
  } catch (e) {} // Use our detect's results. passive applied if supported, capture will be false either way.
  //elem.addEventListener('touchstart', fn, supportsPassive ? { passive: true } : false);

})();

function getSVGIconHTML(name, tag, attrs) {
  if (typeof name === 'undefined') {
    console.error('name is required');
    return false;
  }

  if (typeof tag === 'undefined') {
    tag = 'div';
  }

  var classes = 'svg-icon svg-icon--<%= name %>';
  var iconHTML = ['<<%= tag %> <%= classes %>>', '<svg class="svg-icon__link">', '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#<%= name %>"></use>', '</svg>', '</<%= tag %>>'].join('').replace(/<%= classes %>/g, 'class="' + classes + '"').replace(/<%= tag %>/g, tag).replace(/<%= name %>/g, name);
  return iconHTML;
}
/* ^^^
 * JQUERY Actions
 * ========================================================================== */


$(function () {
  'use strict';
  /**
   * определение существования элемента на странице
   */

  $.exists = function (selector) {
    return $(selector).length > 0;
  };
  /**
   * [^_]*.js - выборка всех файлов, которые не начинаются с подчеркивания
   */


  function documentAddClassOverflowHidden() {
    if (/iPod|iPad|iPhone/i.test(navigator.userAgent)) {
      $('body').addClass('overflow-hidden');
    } else {
      $('body').removeClass('overflow-hidden');
      $(document.documentElement).addClass('overflow-hidden');
    }
  }

  function documentRemoveClassOverflowHidden() {
    $('body').removeClass('overflow-hidden');
    $(document.documentElement).removeClass('overflow-hidden');
  }

  ;

  (function () {
    $('._js_search span').on('click tap', function () {
      $(this).closest('._js_search').toggleClass('opened');
      $(this).closest('._js_search').find('input').focus();
    });
    $('._js_search.mobile input').on('keypress', function () {
      var $this = $(this),
          letLength = $this.val();
      $this.closest('._js_search.mobile').addClass('typing');

      if ($this.val().length < 0) {
        $('._js_search.mobile').removeClass('typing');
      }
    });
    $('._js_search').submit(function (event) {
      if ($(this).find('input').val() == '') {
        event.preventDefault();
        $(this).removeClass('opened');
      }
    });
    $('._js_search.mobile input').on('keyup', function () {
      var $this = $(this),
          letLength = $this.val();

      if ($this.val().length == 0) {
        $('._js_search.mobile').removeClass('typing');
      }
    });
    $('.mobile-panel').css({
      'height': $(window).height() + 60
    });
    $('.app-header__burger, .landing-header__burger').on('click tap', function () {
      $('.mobile-panel').addClass('opened');
      $('.app__overlay').addClass('opened');
      documentAddClassOverflowHidden();
    });
    $('.mobile-panel__close').on('click tap', function () {
      $('.mobile-panel').removeClass('opened');
      $('.app__overlay').removeClass('opened');
      documentRemoveClassOverflowHidden();
    });
    $(document).on('click tap', function (event) {
      if ($(event.target).closest('.app-header__burger, .landing-header__burger, .mobile-panel__content, .toggle-catalog, .vertical, .folders-list__item, .search-form__container, .content-menu .has-levels, .b-filter, .filter-button, .b-filter__checked-param').length) {
        return;
      }

      $('.mobile-panel').removeClass('opened');
      $('.app__overlay').removeClass('opened');
      $('.toggle-catalog').removeClass('is-active');
      $('.vertical').removeClass('opened');
      $('.folders-list__item').removeClass('opened');
      $('._js_search').removeClass('opened');
      $('.b-filter').removeClass('opened');
      documentRemoveClassOverflowHidden();
    });
    $('.app-header__folders-shared .has-sublevels').on({
      mouseover: function mouseover() {
        var $this = $(this),
            index = $this.data('index');
        $('.horisontal .has-sublevels').filter('[data-index="' + index + '"]').addClass('is-active');
        $('.toggle-catalog').addClass('opened');
      },
      mouseleave: function mouseleave() {
        var $this = $(this),
            index = $this.data('index');
        $('.horisontal .has-sublevels').filter('[data-index="' + index + '"]').removeClass('is-active');
        $('.toggle-catalog').removeClass('opened');
      }
    });
    $('.vertical li').each(function () {
      var $this = $(this);

      if ($this.children('ul').length) {
        $this.addClass('has-sublevels');
        $this.children('a').append('<span class="arrow"><svg width="6" height="8" viewBox="0 0 6 8" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M0.734375 0.933333L1.66771 0L5.66771 4L1.66771 8L0.734375 7.06667L3.80104 4L0.734375 0.933333Z" fill="white"/></svg></span>');
      }
    });
    $('.svg-icon--arrow-b').on('click tab', function () {
      $('.search-block__sorting').toggleClass('opened');
    });

    function selectText(elementId) {
      var doc = document,
          text = doc.getElementById(elementId),
          range,
          selection;

      if (doc.body.createTextRange) {
        range = document.body.createTextRange();
        range.moveToElementText(text);
        range.select();
      } else if (window.getSelection) {
        selection = window.getSelection();
        range = document.createRange();
        range.selectNodeContents(text);
        selection.removeAllRanges();
        selection.addRange(range);
      }
    }

    $(".copy").click(function (e) {
      e.preventDefault();
      selectText(this.id);
      document.execCommand("copy");
    });
    $('.vertical.mobile li ul').css({
      'height': $(window).height()
    });
    $('.vertical.mobile li').each(function () {
      var $this = $(this),
          subLevels = $this.find('>ul');

      if (subLevels.length > 0) {
        $this.find('>a').clone().addClass('back').prependTo(subLevels).wrap('<li>');
      }
    });
    $('.vertical.mobile > .has-sublevels > a').on('click tap', function (event) {
      event.preventDefault();
      $(this).parent().addClass('is-opened');
      $(this).parent().find('>ul').addClass('opened');
    });
    $('.back').on('click tap', function (event) {
      event.preventDefault();
      $(this).closest('ul').removeClass('opened');
      $(this).closest('.has-sublevels').removeClass('is-opened');
    });
    var docSrollFlag = 1; // $(document).scroll(function(){
    // 	if($(window).width() > 1023) {
    // 		if($(document).scrollTop() > 0 && docSrollFlag == 1) {
    // 			$('.app-header').addClass('scroll')
    // 			docSrollFlag = 2
    // 		} else if($(document).scrollTop() <= 0 && docSrollFlag == 2) {
    // 			$('.app-header').removeClass('scroll')
    // 			docSrollFlag = 1
    // 		}
    // 	}
    // })

    $(document).scroll(function () {
      if ($(window).width() > 1023) {
        if ($(document).scrollTop() > $('.app-header__top-container').height() + $('.app-header__bot').height() && docSrollFlag == 1) {
          $('.app-header').addClass('scroll');
          $('.app-header__logo').appendTo('.app-header__fc-l');
          $('.toggle-catalog').appendTo('.app-header__fc-l');
          $('.app-header__middle .search-form._js_search').appendTo('.app-header__fc-l');
          $('.app-header__button.s-button.type-2').appendTo('.app-header__fc-l');
          $('.app-header__e-mail').appendTo('.app-header__fixed-container-in');
          $('.app-header__phone-item').appendTo('.app-header__fixed-container-in');
          $('.app-header__cart').appendTo('.app-header__fixed-container-in'); // $('.has-sublevels ul').height($(window).height() - 89)	

          $('.vertical.desctop').addClass('fix');

          if ($('.vertical.desctop.fix').innerHeight() - 9 > $(window).height()) {
            $('.vertical.desctop.fix').css({
              'height': $(window).height() - 89
            });
          }

          if ($('.vertical.desctop.fix > .has-sublevels > ul').innerHeight() - 9 > $(window).height()) {
            $('.vertical.desctop.fix > .has-sublevels > ul').css({
              'height': $(window).height() - 89,
              'overflow-y': 'auto'
            });
          }

          $('.vertical.desctop.fix > .has-sublevels > ul').css({
            'top': 89,
            'left': $('.vertical.desctop.fix').offset().left + 335
          });
          docSrollFlag = 2;
        } else if ($(document).scrollTop() <= $('.app-header__top-container').height() + $('.app-header__bot').height() && docSrollFlag == 2) {
          $('.app-header').removeClass('scroll');
          $('.app-header__logo').prependTo('.app-header__middle');
          $('.toggle-catalog').prependTo('.horisontal');
          $('.app-header__middle .search-form._js_search').appendTo('.app-header__fixed-container-in');
          $('.app-header__menu').after($('.app-header__e-mail'));
          $('.app-header__phone-item').appendTo('.app-header__phone');
          $('.app-header__button.s-button.type-2').appendTo('.app-header__middle');
          $('.search-form._js_search').appendTo('.app-header__middle');
          $('.app-header__cart').appendTo('.app-header__middle');
          $('.vertical.desctop').removeClass('fix');
          $('.vertical.desctop .has-sublevels > ul').css({
            'top': 0,
            'left': 100 + '%',
            'overflow-y': 'visible'
          });
          docSrollFlag = 1;
        }
      }
    });
  })();

  $('.input_range_slider').each(function (index, el) {
    var $this = $(this),
        $lower = $this.closest('.range_slider_wrapper').find('.low'),
        $upper = $this.closest('.range_slider_wrapper').find('.hight'),
        inputRangeMax = $upper.data('val'),
        inputRangeMin = $lower.data('val'),
        placeHolder = $this.closest('.range_slider_wrapper').find('.b-filter__field input'),
        arr = [inputRangeMin, inputRangeMax];

    if ($(window).width() > 1023) {
      var slider = $this.noUiSlider({
        // start: [$lower.attr('value'), $upper.attr('value')],
        start: [inputRangeMin, inputRangeMax],
        connect: true,
        behaviour: 'drag-tap',
        format: wNumb({
          decimals: 0
        }),
        range: {
          'min': [arr[0]],
          'max': [arr[1]]
        }
      });
      slider.Link('lower').to($lower);
      slider.Link('upper').to($upper);
      $('.noUi-handle.noUi-handle-lower').append('<span class="s-from-span">' + $lower.val().replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1 ') + '</span>');
      $('.noUi-handle.noUi-handle-upper').append('<span class="s-to-span">' + $upper.val().replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1 ') + '</span>');
      slider.on('slide', function () {
        var newLoverValue = $lower.val(),
            newUpperValue = $upper.val();
        $this.find('.s-from-span').text($lower.val().replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1 '));
        $this.find('.s-to-span').text($upper.val().replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1 '));
      });
      slider.on('set.one', function () {
        var newLoverValue = $lower.val(),
            newUpperValue = $upper.val();
        $this.find('.s-from-span').text($lower.val().replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1 '));
        $this.find('.s-to-span').text($upper.val().replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1 '));
      });
    }

    var inputVal = "";
    placeHolder.keyup(function () {
      $(this).addClass('tpp');

      if ($('.tpp').length == 2) {
        $('.b-filter__reset-price').show();
      }
    });
    $('.b-filter__reset-price').on('click tap', function () {
      $lower.val(inputRangeMin);
      $upper.val(inputRangeMax);
      placeHolder.removeClass('tpp');
      $('.b-filter__reset-price').hide();
    });
    placeHolder.focus(function () {
      var $this = $(this);
      inputVal = $this.val(); // $this.val('')
    });
    placeHolder.focusout(function () {
      var $this = $(this);

      if ($this.val() == '') {
        $this.val(inputVal);
      }
    });
  });
  $('.b-filter__item.toggle .b-filter__item-name').on('click tap', function () {
    if ($(window).width() > 1023) {
      $(this).closest('.b-filter__item').find('.b-filter__item-params-list').slideToggle();
      $(this).closest('.b-filter__item').toggleClass('opened');
    }
  });
  $('.filter-button').on('click tap', function () {
    $('.b-filter').toggleClass('opened');
    documentAddClassOverflowHidden();
  });
  $('.b-filter__item-name').on('click tap', function () {
    var $this = $(this);

    if (!$this.closest('.b-filter__item').hasClass('b-filter__item--single')) {
      $this.closest('.b-filter__item-params-container').toggleClass('opened');
      $('.b-filter').toggleClass('no-scroll');
    }
  });

  function testParams() {
    if ($('.b-filter__selected-container').children().length == 0) {
      $('.b-filter__sub-title').removeClass('show');
    } else {
      $('.b-filter__sub-title').addClass('show');
    }
  }

  $(document).on('click tap', '._js_p-elem', function () {
    var $this = $(this),
        paramsContainer = $this.closest('.b-filter__item-params-container'),
        listIndex = paramsContainer.data('list-index'),
        itemIndex = $this.closest('.b-filter__item-param').data('item-index'),
        parentName = $this.closest('.b-filter__item-params-list').find('.b-filter__item-name').text(),
        name = $this.closest('.b-filter__item-param').find('span').text();
    paramsContainer.addClass('selected show-btn');

    if ($('.b-filter__selected-container .b-filter__checked-param').filter('[data-list-index="' + listIndex + '"]').length <= 0) {
      $('<div class="b-filter__checked-param"' + ' data-list-index="' + listIndex + '"><span class="remove-item"></span><div class="parent-name">' + parentName + '</div><div class="items-container" data-list-index="' + listIndex + '"></div></div>').appendTo('.b-filter__selected-container');
    }

    if ($this.hasClass('checked') && !$this.closest('.b-filter__item-param').hasClass('cloned') && !$this.closest('.b-filter__item').hasClass('b-filter__item--single')) {
      $('.b-filter__checked-param').filter('[data-list-index="' + listIndex + '"]').find('.items-container').append('<span class="item-name" data-item-index="' + itemIndex + '">' + name + '</span>');
    } else {
      $('.b-filter__checked-param').filter('[data-list-index="' + listIndex + '"]').find('.item-name').filter('[data-item-index="' + itemIndex + '"]').remove();
    }

    if ($('.b-filter__checked-param').filter('[data-list-index="' + listIndex + '"]').find('.items-container').children('.item-name').length == 0) {
      $('.b-filter__checked-param').filter('[data-list-index="' + listIndex + '"]').remove();
      paramsContainer.removeClass('selected');
    }

    testParams();
    $('.b-filter').addClass('show-submit-btn');
  });
  $(document).on('click tap', '.b-filter__item-param.cloned ._js_p-elem', function () {
    $(this).toggleClass('checked');
    $(this).parent().toggleClass('checked');
  });
  $(document).on('click', '.items-container', function () {
    var $this = $(this),
        listIndex = $this.data('list-index');
    $('.b-filter__item-params-container').filter('[data-list-index="' + listIndex + '"]').addClass('opened').removeClass('show-btn');
  });
  $('.b-filter__back-btn, .b-filter__item-name--mobile').on('click tap', function () {
    var $this = $(this),
        container = $this.closest('.b-filter__item-params-list').find('.b-filter__for-checked-items-container'),
        itemsContainer = container.find('.b-filter__selected-items-container');
    setTimeout(function () {
      $('.b-filter__selected-items-container .b-filter__item-param').each(function () {
        if (!$(this).hasClass('checked')) {
          $(this).remove();
        }
      });
      container.addClass('show');
      $('.b-filter__list-box .b-filter__item-param.checked').not(':hidden').hide().clone(true).show().appendTo(itemsContainer).addClass('cloned');
      $('.b-filter__item-param').not('.checked').show();

      if (container.find('.b-filter__selected-items-container').children().length == 0) {
        container.removeClass('show');
      } else {
        container.addClass('show');
      }

      showTotalParams();
    }, 400);
    $this.closest('.b-filter__item-params-container').removeClass('opened');
  });
  $(document).on('click tap', '.remove-item', function () {
    var $this = $(this),
        listIndex = $this.closest('.b-filter__checked-param').data('list-index');

    if ($this.closest('.b-filter__checked-param').hasClass('single')) {
      $this.closest('.b-filter__checked-param');
      $this.closest('.b-filter__checked-param').find('.b-filter__item-param').appendTo($('.b-filter__item-params-container').filter('[data-list-index="' + listIndex + '"]')).find('._js_p-elem-single').removeClass('checked').prop('checked', false);
      $('.b-filter__item-params-container').filter('[data-list-index="' + listIndex + '"]').closest('.b-filter__item').show();
      $('.b-filter__checked-param').filter('[data-list-index="' + listIndex + '"]').remove();
    } else {
      $this.closest('.b-filter__checked-param').remove();
      $('.b-filter__item-params-container').filter('[data-list-index="' + listIndex + '"]').removeClass('selected').find('.b-filter__for-checked-items-container').removeClass('show').children('.b-filter__item-param').remove();
      $('.b-filter__item-params-container').filter('[data-list-index="' + listIndex + '"]').find('.b-filter__item-param').removeClass('checked').show().find('._js_p-elem').removeClass('checked').find('input').prop('checked', false);
    }

    testParams();
    $('.b-filter').addClass('show-submit-btn');
  });
  $('.b-filter__items-reset').on('click tap', function () {
    var $this = $(this),
        container = $this.closest('.b-filter__item-params-container'),
        listIndex = container.data('list-index');
    container.removeClass('selected').addClass('show-btn');
    container.find('.b-filter__for-checked-items-container').removeClass('show');
    container.find('.b-filter__selected-items-container').children().remove();
    container.find('.b-filter__item-param').removeClass('checked').show().find('._js_p-elem').removeClass('checked').find('input').prop('checked', false);
    $('.b-filter__selected-container .b-filter__checked-param').filter('[data-list-index="' + listIndex + '"]').remove();
    testParams();
    $('.b-filter').addClass('show-submit-btn');
  });
  $('.reset-all').on('click tap', function () {
    $('.b-filter__selected-container ._js_p-elem-single').each(function () {
      var $this = $(this),
          listIndex = $this.closest('.b-filter__checked-param').data('list-index');
      $this.removeClass('checked').prop('checked', false);
      $this.closest('.b-filter__item-param').appendTo($('.b-filter__item-params-container').filter('[data-list-index="' + listIndex + '"]')); // $this.closest('.b-filter__item-param').appendTo($('.b-filter__item-params-container').filter('[data-list-index="' + listIndex + '"]'))
    });
    $('.b-filter__item--single').show();
    $('.b-filter__checked-param').remove();
    $('.b-filter__sub-title').removeClass('show');
    $('.b-filter__item-params-container').removeClass('selected show-btn');
    $('.b-filter__for-checked-items-container').removeClass('show').find('.b-filter__item-param').remove();
    $('.b-filter__item-param').removeClass('checked').show().find('._js_p-elem').removeClass('checked').find('input').prop('checked', false);
    $('.b-filter').addClass('show-submit-btn');
  });

  function checkedParams() {
    $('._js_p-elem-single').each(function () {
      var $this = $(this);

      if ($this.is(':checked')) {
        var name = $this.closest('.b-filter__item').find('.b-filter__item-name').text(),
            listIndex = $this.closest('.b-filter__item-params-container').data('list-index'),
            param = $this.closest('.b-filter__item-param');
        $this.closest('.b-filter__item--single').hide();
        $('<div class="b-filter__checked-param checked single"' + ' data-list-index="' + listIndex + '"><span class="remove-item"></span><div class="parent-name">' + name + '</div></div>').appendTo('.b-filter__selected-container');

        if ($('.b-filter__checked-param').filter('[data-list-index="' + listIndex + '"]').children('.b-filter__item-param').length <= 0) {
          param.addClass('single').appendTo($('.b-filter__checked-param').filter('[data-list-index="' + listIndex + '"]'));
        }

        testParams();
      } else {
        var listIndex = $this.closest('.b-filter__checked-param').data('list-index');
        $this.closest('.b-filter__item-param').appendTo($('.b-filter__item-params-container').filter('[data-list-index="' + listIndex + '"]'));
        $('.b-filter__item-params-container').filter('[data-list-index="' + listIndex + '"]').closest('.b-filter__item').show();
        $('.b-filter__checked-param').filter('[data-list-index="' + listIndex + '"]').remove();
        testParams();
      }
    });
  }

  checkedParams();
  $(document).on('click tap', '._js_p-elem-single', function () {
    $('.b-filter').addClass('show-submit-btn');
  });
  $('.b-filter__button').on('click tap', function (evet) {
    evet.preventDefault();
    $('.b-filter').removeClass('opened show-submit-btn');
    $('html').removeClass('overflow-hidden');
    checkedParams();
    showTotalParams();
  });

  function showTotalParams() {
    if ($('.b-filter__checked-param.checked.single').length + $('.b-filter__list-box .b-filter__item-param.checked').length > 0) {
      $('.pr-total').addClass('show');
    } else {
      $('.pr-total').removeClass('show');
    }

    $('.pr-total').text($('.b-filter__checked-param.checked.single').length + $('.b-filter__list-box .b-filter__item-param.checked').length);
  }

  showTotalParams(); // function validateEmail($email) {
  //  	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  //  	return emailReg.test( $email );
  // }

  $('._js_selected_country').on('click tap', function () {
    $(this).parent().toggleClass('opened');
  });
  $('._js_select_country div').on('click tap', function () {
    var $this = $(this),
        phoneNumber = $this.data('phone-number'),
        placeholder = phoneNumber.slice(0, 2);
    $this.closest('.b-form__item--phone').find('._js_phone-number').val(placeholder); // .mask(phoneNumber);

    $('.b-form__country-flag').removeClass('opened');
  });
  $('[type=tel]').bind("change keyup input click", function () {
    if (this.value.match(/[^0-9,+]/g)) {
      this.value = this.value.replace(/[^0-9,+]/g, '');
    }
  }); // $('input[type=tel]').mask("+7 (999) 99 99 99");
  // $('input[type=tel]').inputmask('(99) 9999[9]-9999');

  $('.b-form__item-close').on('click tap', function () {
    $(this).closest('.b-form__item').removeClass('error');
  });
  $('input[type="email"]').focusout(function () {
    if ($(this).closest('.b-form').find('input[type="email"]:invalid').length) {
      $(this).closest('.b-form').find('input[type="email"]').closest('.b-form__item').addClass('error');
    }
  });
  $('.b-form .b-form__button').on('click', function (event) {
    if ($(this).closest('.b-form').find('input[type="email"]:invalid').length) {
      $(this).closest('.b-form').find('input[type="email"]').closest('.b-form__item').addClass('error');
    } else {
      $(this).closest('.b-form').find('input[type="email"]').closest('.b-form__item').removeClass('error');
    }

    if ($(this).hasClass('disabled') || $(this).closest('.b-form').find('input[type="tel"]').val().length < 12 || $(this).closest('.b-form').find('input[type="email"]').val() == "") {
      return false;
    }
  });
  $('._js_b-slider').owlCarousel({
    items: 1,
    loop: true,
    autoplay: true,
    margin: 0,
    nav: true,
    dots: true,
    // autoHeight: true,
    mouseDrag: false,
    animateOut: 'fadeOut',
    animateIn: 'slideOutin',
    navText: ['', '']
  });
  $('.owl-prev').append('<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20"><path d="M13.25 10l-7.141-7.42c-0.268-0.27-0.268-0.707 0-0.979 0.268-0.27 0.701-0.27 0.969 0l7.83 7.908c0.268 0.271 0.268 0.709 0 0.979l-7.83 7.908c-0.268 0.271-0.701 0.27-0.969 0s-0.268-0.707 0-0.979l7.141-7.417z"></path></svg>');
  $('.owl-next').append('<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20"><path d="M13.25 10l-7.141-7.42c-0.268-0.27-0.268-0.707 0-0.979 0.268-0.27 0.701-0.27 0.969 0l7.83 7.908c0.268 0.271 0.268 0.709 0 0.979l-7.83 7.908c-0.268 0.271-0.701 0.27-0.969 0s-0.268-0.707 0-0.979l7.141-7.417z"></path></svg>');
  $('.b-card-tabs__titles-item > a').on('click', function (e) {
    e.preventDefault();
    var $this = $(this),
        li = $this.closest('li'),
        id = $this.attr('href');
    $('.b-card-tabs__titles-item').removeClass('active');
    li.addClass('active');
    $('.b-card-tabs__body').removeClass('active');
    $(id).addClass('active');
  });
  $('.b-card-tabs__body-name').on('click', function (e) {
    e.preventDefault();
    var $this = $(this),
        id = $this.closest('.b-card-tabs__body'),
        li = $('.b-card-tabs__titles-item:eq(' + id.index() + ')');

    if (id.hasClass('active')) {
      $('.b-card-tabs__titles-item').removeClass('active');
      $('.b-card-tabs__body').removeClass('active');
    } else {
      $('.b-card-tabs__titles-item').removeClass('active');
      li.addClass('active');
      $('.b-card-tabs__body').removeClass('active');
      id.addClass('active');
    }

    var indent = parseInt($('.app-header__top').outerHeight());
    $('html, body').animate({
      scrollTop: $this.offset().top - indent
    }, 600);
  });
  var cientTypeRadios = $('.cart-client-type [type="radio"]');
  var lawyerFields = $('.lawyer-fields');
  cientTypeRadios.on('change', function () {
    if (cientTypeRadios.filter(":checked").val() == 2) {
      lawyerFields.stop().slideDown();
    } else {
      lawyerFields.stop().slideUp();
    }
  });
  var $cartOrder = $('.cart-order');
  var $cartListToggle = $('.cart-list-toggle');
  $('.cart-order-form-overlap').on('click', function (event) {
    event.preventDefault();
    $cartOrder.toggleClass('_js-is-overlap');
  });
  $('.lawyer-fields-handy-btn').on('click', function (event) {
    event.preventDefault();
    $(this).toggleClass('is-opened');
    $('.lawyer-fields-handy').slideToggle();
  });
  var getProductMethosRadios = $('.new-cart-form-radio-button-group [type="radio"]');
  var courierFields = $('.courier-fields');
  getProductMethosRadios.on('change', function () {
    if (getProductMethosRadios.filter(":checked").val() == 2) {
      courierFields.stop().slideDown();
    } else {
      courierFields.stop().slideUp();
    }
  });
  var $cartTable = $('.cart-table');
  var $cartTableWrap = $cartTable.closest('.cart-order__table-wrap');
  var $cartTableRows = $cartTable.find('.cart-table__body-row');
  var $slideCartTable = $('.cart-table-slide');

  function getFirstThreeRowsHeight(rows) {
    return rows.eq(0).outerHeight(true) + rows.eq(1).outerHeight(true) + rows.eq(2).outerHeight(true);
  }

  function setCartTableHeight() {
    console.log(getFirstThreeRowsHeight($cartTableRows));
    $cartTableWrap.css({
      height: getFirstThreeRowsHeight($cartTableRows)
    });
  }

  if ($cartTableRows.length > 3) {
    setCartTableHeight();
    $(window).on('resize', setCartTableHeight);
  }

  $slideCartTable.on('click', function (event) {
    event.preventDefault();

    if ($(this).hasClass('is-opened')) {
      $(this).removeClass('is-opened');
      $cartTableWrap.animate({
        height: getFirstThreeRowsHeight($cartTableRows)
      }, {
        queue: false,
        duration: 500
      });
    } else {
      $(this).addClass('is-opened');
      $cartTableWrap.animate({
        height: $cartTable.outerHeight(true)
      }, {
        queue: false,
        duration: 300
      });
    }
  });
  $('.b-categor-filter__item').on('click', function (e) {
    e.preventDefault();
    var filterId = $(this).data('filter');
    $('.b-categor-block__li').removeClass('hide');

    if (filterId != 'all') {
      $('.b-categor-block__ul').each(function () {
        var $thisUl = $(this),
            filteLi = $thisUl.find('.b-categor-block__li[data-categ="' + filterId + '"]');
        $thisUl.find('.b-categor-block__li').not(filteLi).addClass('hide').appendTo($thisUl);
      });
    }

    $('.b-categor-filter__item').removeClass('active');
    $(this).addClass('active');
  });
  $('.b-categor-lm__ul > li').each(function () {
    var $this = $(this);

    if ($this.find('> ul > li').length > 2) {
      var viewall = $('<li class="b-categor-lm__viewAll"><a href="#">Еще <span class="svg-icon svg-icon--arrow-b"><svg class="svg-icon__link"><use xlink:href="#arrow-b"></use></svg></span></a></li>'),
          ul = $this.find('> ul');
      viewall.appendTo(ul);
    }
  });
  $(document).on('click', '.b-categor-lm__viewAll > a', function (e) {
    e.preventDefault();
    $(this).toggleClass('opened').closest('ul').find('li').not('.b-categor-lm__viewAll, li:nth-child(1), li:nth-child(2)').slideToggle(400);
  }); // let compareTableProducts = $('.compare-table__products');
  // let compareFeatures = $('.compare-features');
  // compareFeatures.css({
  //   paddingBottom: $(compareTableProducts).outerHeight() - compareProductsList.height();
  // });

  var compareProductsList = $('.compare-products-list');
  compareProductsList.addClass('owl-carousel').owlCarousel({
    items: 4,
    margin: 0,
    dots: false,
    nav: true,
    responsive: {
      0: {
        items: 1
      },
      540: {
        items: 2
      },
      720: {
        items: 3
      },
      1140: {
        items: 4
      }
    }
  });

  if ($('#contacts-page-map').length) {
    var init = function init() {
      // Поиск координат
      ymaps.geocode(myAddress, {
        results: 1
      }).then(function (res) {
        // Выбираем первый результат геокодирования
        var firstGeoObject = res.geoObjects.get(0);
        var cords = firstGeoObject.geometry.getCoordinates();
        centerMap = cords;
      }, function (err) {
        // Если геокодирование не удалось,
        // сообщаем об ошибке
        alert(err.message);
      });
      myMap = new ymaps.Map('contacts-page-map', {
        center: centerMap,
        zoom: 14
      }); //Добавляем элементы управления	

      myMap.controls.add('zoomControl').add('typeSelector').add('mapTools');
      var myPlacemark = new ymaps.Placemark(centerMap, {
        hintContent: myAddress,
        balloonContent: myAddress
      }, {
        // Опции.
        // Необходимо указать данный тип макета.
        iconLayout: 'default#image',
        // Своё изображение иконки метки.
        iconImageHref: '../img/logomap.svg',
        // Размеры метки.
        iconImageSize: [128, 54],
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [-120, -54]
      });
      markers.push(myPlacemark);
      myMap.geoObjects.add(myPlacemark); //Отсеживаем событие клика по карте		

      myMap.events.add('click', function (e) {
        var coords = e.get('coordPosition');

        if (markers.length < 10) {
          if (markers.length >= 1) {
            reset();
            var myPlacemark = new ymaps.Placemark(centerMap, {
              hintContent: myAddress,
              balloonContent: myAddress
            }, {
              // Опции.
              // Необходимо указать данный тип макета.
              iconLayout: 'default#image',
              // Своё изображение иконки метки.
              iconImageHref: '../img/logomap.svg',
              // Размеры метки.
              iconImageSize: [128, 54],
              // Смещение левого верхнего угла иконки относительно
              // её "ножки" (точки привязки).
              iconImageOffset: [-120, -54]
            });
            markers.push(myPlacemark);
            myMap.geoObjects.add(myPlacemark);
            ch = 2;
            myPlacemark = new ymaps.Placemark([coords[0].toPrecision(6), coords[1].toPrecision(6)], {
              // Свойства
              // Текст метки
              iconContent: ch
            }, {
              // Опции
              // Иконка метки будет растягиваться под ее контент
              preset: 'twirl#blueStretchyIcon'
            });
            markers.push(myPlacemark);
            myMap.geoObjects.add(myPlacemark);
            ch++;
            calcRoute();
          }
        } else {
          alert("Вы задали максимальное количество точек");
        }
      });
    };

    var calcRoute = function calcRoute() {
      for (var i = 0, l = markers.length; i < l; i++) {
        point[i] = markers[i].geometry.getCoordinates();
      }

      ymaps.route(point, {
        // Опции маршрутизатора
        mapStateAutoApply: true // автоматически позиционировать карту

      }).then(function (router) {
        route = router;
        myMap.geoObjects.add(route);
      }, function (error) {
        alert("Возникла ошибка: " + error.message);
      });
    }; //Удаление маршрута и меток с карты и очистка данных


    var reset = function reset() {
      route && myMap.geoObjects.remove(route);

      for (var i = 0, l = markers.length; i < l; i++) {
        myMap.geoObjects.remove(markers[i]);
      }

      markers = [];
      point = [];
      ch = 1;
    };

    var myMap,
        route,
        ch = 1,
        markers = [],
        point = [];
    var myAddress = 'г. Москва, ул. Электродная, д.2, стр.7',
        centerMap = $('#contacts-page-map').data('center').split(',');
    ymaps.ready(init);
  }

  $('.content-menu > ul > li').each(function () {
    var $this = $(this),
        subLevels = $this.find('>ul');

    if ($this.children('ul').length) {
      $this.find('>a').clone().addClass('back').prependTo(subLevels).wrap('<li>').find('.icon').remove();
    }
  });
  $('.content-menu > ul > li > a').on('click tap', function (event) {
    var subLevels = $(this).find('>ul');

    if ($(this).parent().children('ul').length) {
      event.preventDefault();
      documentAddClassOverflowHidden();
      $(this).parent().addClass('opened');
    }
  });
  $('.content-menu ul li .back').on('click tap', function (event) {
    event.preventDefault();
    $(this).closest('.has-levels').removeClass('opened');
    documentRemoveClassOverflowHidden();
  });
  $('.copy-compare-link__action').click(function (e) {
    e.preventDefault();
    $('#' + $(this).data('id')).select();
    document.execCommand("copy");
  });

  function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  $('.floated-input__input').on('input', function () {
    var $self = $(this);
    var $parent = $self.closest('.floated-input');
    var $validator = $self.attr('type');

    if ($self.val().length > 0) {
      $parent.addClass('is-filled');

      if ($validator == 'email') {
        if (validateEmail($self.val())) {
          $parent.removeClass('is-error').addClass('is-success');
        } else {
          $parent.removeClass('is-success').addClass('is-error');
        }
      }
    } else {
      $parent.removeClass('is-filled is-error is-valid');
    }
  }); // $('.folders-list__item').on('click tap', function(event){
  // 	var $this = $(this);
  // 	event.preventDefault();
  // 	if($(window).width() <= 640) {
  // 		$this.toggleClass('opened')
  // 		$this.find('.folders-list__item-list').slideToggle()
  // 	} else {
  // 		if($this.hasClass('opened')) {
  // 			$this.removeClass('opened')
  // 		} else {
  // 			$('.folders-list__item').removeClass('opened')
  // 			$this.addClass('opened')
  // 		}
  // 	}
  // })

  if ($('.b-form--landing .b-form__item input[type=file]').length) {
    $('.b-form--landing .b-form__item input[type=file]').each(function (index, el) {
      var filePlaceholder = $(this).data('placeholder');
      $(this).styler({
        filePlaceholder: filePlaceholder
      });
    });
  }

  if ($('.landing-header--fixed').length) {
    var maxHeight = 0;
    $(window).on('scroll', function () {
      var old_scroll = $(window).scrollTop();

      if (old_scroll > maxHeight) {
        $(".landing-header--fixed").addClass('scroller');
      } else {
        $(".landing-header--fixed").removeClass('scroller');
      }
    });
  }

  if ($('.landing-header__menu').length) {
    $('.landing-header__menu').appendTo('.mobile-panel__content').removeClass('landing-header__menu--hidden');
  }

  $('.metro-select__button').on('click', function () {
    $(this).closest('.metro-select').toggleClass('is-opened');
  });
  $('.metro-name').on('click', function () {
    var $this = $(this);
    var $select = $this.closest('.metro-select');
    $this.addClass('metro-name--checked').siblings().removeClass('metro-name--checked');
    $select.toggleClass('is-opened').find('.metro-select__label div').text($this.text());
  });
  $(document).on('click', function (event) {
    if ($(event.target).closest('.metro-select').length < 1) {
      $('.metro-select').removeClass('is-opened');
    }
  });
  ;

  (function ($) {
    'use strict';

    var PAGE = $('html, body');
    var pageScroller = $('.page-scroller'),
        pageYOffset = 0,
        inMemory = false,
        inMemoryClass = 'page-scroller--memorized',
        isVisibleClass = 'page-scroller--visible',
        enabledOffset = 60;

    function resetPageScroller() {
      setTimeout(function () {
        if (window.pageYOffset > enabledOffset) {
          pageScroller.addClass(isVisibleClass);
        } else if (!pageScroller.hasClass(inMemoryClass)) {
          pageScroller.removeClass(isVisibleClass);
        }
      }, 150);

      if (!inMemory) {
        pageYOffset = 0;
        pageScroller.removeClass(inMemoryClass);
      }

      inMemory = false;
    }

    if (pageScroller.length > 0) {
      window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
        passive: true
      } : false);
      pageScroller.on('click', function (event) {
        event.preventDefault();
        window.removeEventListener('scroll', resetPageScroller);

        if (window.pageYOffset > 0 && pageYOffset === 0) {
          inMemory = true;
          pageYOffset = window.pageYOffset;
          pageScroller.addClass(inMemoryClass);
          PAGE.stop().animate({
            scrollTop: 0
          }, 500, 'swing', function () {
            window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
              passive: true
            } : false);
          });
        } else {
          pageScroller.removeClass(inMemoryClass);
          PAGE.stop().animate({
            scrollTop: pageYOffset
          }, 500, 'swing', function () {
            pageYOffset = 0;
            window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
              passive: true
            } : false);
          });
        }
      });
    }
  })(jQuery);

  $('._js-scroll-to-block').on('click', function (event) {
    event.preventDefault();
    var scrollId = $(this).data('scroll');
    var pageOffset = $('.app-header__top-container').outerHeight();
    var scrollPosition = $('#' + scrollId).offset().top - pageOffset;
    $('body,html').stop().animate({
      scrollTop: scrollPosition
    }, 500);
  });
  var inst3 = $('[data-remodal-id=modal3]').remodal();
  var inst4 = $('[data-remodal-id=modal4]').remodal();
  $('.popup-choose-city__city').on('click tap', function () {
    $('.app-header__phone-sity > a').text($(this).text());
    $('.app-header__phone-item > a').text($(this).data('phone'));
    $('.app-header__phone-item > a').attr('href', 'tel:' + $(this).data('phone'));
    inst3.close();
  });
  $('.chiose-city.remodal').parent().addClass('chiose-city-wrapper');
  $('.choose-bottom.remodal').parent().addClass('choose-bottom-wrapper');
  $('.popup-choose-city__form input').on('keyup', function () {
    if ($(this).val() == "") {
      $('.popup-choose-city__form-dropdown-list').removeClass('show');
    } else {
      $('.popup-choose-city__form-dropdown-list').addClass('show');
    }
  });
  $('.choose-bottom .type-8.remodal-cancel').on('click', function () {
    setTimeout(function () {
      inst3.open();
    }, 800);
  });
  var inst1 = $('[data-remodal-id=modal1]').remodal();
  var inst2 = $('[data-remodal-id=modal2]').remodal();
  $('.remodal-close').append('<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="22.9336" y="0.157715" width="1.28827" height="32.2067" transform="rotate(45 22.9336 0.157715)" fill="#3C4858"/><rect x="0.160156" y="1.06885" width="1.28827" height="32.2067" transform="rotate(-45 0.160156 1.06885)" fill="#3C4858"/></svg>');
  $(document).on('click tap', '.remodal-close', function () {
    return false; // $(window).scroll(function() { return false; })
  });
  $('.form.application').parent('.remodal-overlay').addClass('application-wrapper');
  $(window).on('resize load', function () {
    $('.form.application .b-form ').css({
      'height': $(window).height()
    });
  });
  $('input[type="checkbox"]').styler();
  $('.type-1.has-file input[type="file"]').styler({
    filePlaceholder: 'Прикрепить реквизиты',
    fileBrowse: "Обзор..."
  });

  var file_onchange = function file_onchange() {
    var extension = $(this).val().split('.').pop().toLowerCase();
    var maxSize = 10485760;

    if ($.inArray(extension, ['doc', 'docx', 'xls', 'xlsx', 'pdf']) == -1) {
      $(this).closest('.has-file').addClass('error expansion').removeClass('size');
    } else if (this.files[0].size > 10485760) {
      $(this).closest('.has-file').addClass('error size').removeClass('expansion');
      console.log(2);
    } else {
      console.log(3);
    }
  };

  $('.b-form input[type="file"]').on('change', file_onchange);
  $(document).on('click tap', '.b-form__item--name .jq-file__refresh', function () {
    $(this).closest('.b-form__item--name').removeClass('error');
    $('.b-form__item--name input[type="file"]').trigger('refresh');
    $('.b-form__item--name .jq-file__name').text('Прикрепить реквизиты');
    $('.b-form__item--name .jq-file').append('<div class="jq-file__refresh">удалить</div>');
  });
  $('.b-form__item.b-form__item--file input[type="file"]').styler({
    filePlaceholder: 'Просто перетащите сюда файл с заявкой из вашего компьютера или',
    fileBrowse: "Прикрепить файл"
  });
  $('.type-2.has-file input[type="file"]').styler({
    filePlaceholder: 'Прикрепить вашу ЗАЯВКУ',
    fileBrowse: "Прикрепить файл"
  });
  $(document).on('click tap', '.b-form__item--file .jq-file__refresh', function () {
    $(this).closest('.b-form__item--file').removeClass('error');
    $('.b-form__item--file input[type="file"]').trigger('refresh');
    $('.b-form__item--file .jq-file__name').text('Просто перетащите сюда файл с заявкой из вашего компьютера или');
    $('.b-form__item--file .jq-file').append('<div class="jq-file__refresh">удалить</div>');
  });
  $(document).on('click tap', '.type-1.has-file .jq-file__refresh', function () {
    $(this).closest('.has-file').removeClass('error');
    $('.type-1.has-file input[type="file"]').trigger('refresh');
    $('.type-1.has-file .jq-file__name').text('Прикрепить реквизиты');
    $('.type-1.has-file .jq-file').append('<div class="jq-file__refresh">удалить</div>');
  });
  $(document).on('click tap', '.type-2.has-file .jq-file__refresh', function () {
    $(this).closest('.has-file').removeClass('error');
    $('.type-2.has-file input[type="file"]').trigger('refresh');
    $('.type-2.has-file .jq-file__name').text('Прикрепить вашу ЗАЯВКУ');
    $('.type-2.has-file .jq-file').append('<div class="jq-file__refresh">удалить</div>');
  });
  $('.has-file .jq-file').append('<div class="jq-file__refresh">удалить</div>');
  $('.b-form__item--phone input').on('keyup', function () {
    var $this = $(this),
        inputValue = $this.val();

    if (inputValue.length >= 12) {
      $this.addClass('true');
      $this.closest('.b-form').find('.b-form__item--phone').addClass('valid');
    } else {
      $this.removeClass('true');
      $this.closest('.b-form').find('.b-form__item--phone').removeClass('valid');
    }
  });

  function validateForm() {
    var item = $('.application .b-form .b-form__item'),
        inputName = $('.application .b-form .inputName').val(),
        inputEmail = $('.application .b-form .inputEmail').val(),
        inputPhone = $('.application .b-form .b-form__item--phone input'),
        inputAgreemet = $('.application .b-form .b-form__item--greement input');

    if (inputName != "" && inputEmail != "" && inputPhone.hasClass('true') && inputAgreemet.is(':checked') == true && $('.application .b-form .b-form__item.error').length == 0) {
      $('.application .b-form .b-form__button').removeClass('disabled');
    } else {
      $('.application .b-form .b-form__button').addClass('disabled');
    }
  }

  setInterval(function () {
    validateForm();
  }, 200);
  var productSlider = $('.b-product-card__big-image'),
      productItems = productSlider.find('.b-product-card__big-image-item'),
      productThumbs = $('.b-product-card__dop-images'),
      isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || /[\?&]panel_fake_mobile=1(&|$)/.test(document.location.search);
  $.each(productItems, function (i, el) {
    var classActive = i == 0 ? 'is-active' : '';
    var item = $('<div class="b-product-card__dop-images-item ' + classActive + '" data-index="' + i + '"><img src="' + $(el).data('thumb') + '" alt=""></div>');
    item.appendTo(productThumbs);
  });
  productSlider.addClass('owl-carousel').owlCarousel({
    mouseDrag: false,
    items: 1,
    animateOut: 'fadeOut',
    nav: false,
    onChanged: function onChanged(event) {
      var $thisIndex = event.item.index;
      $('.b-product-card__dop-images-item').removeClass('is-active');
      $('.b-product-card__dop-images-item[data-index="' + $thisIndex + '"]').addClass('is-active');
    },
    onInitialize: function onInitialize(event) {
      productThumbs.addClass('owl-carousel').owlCarousel({
        items: 4,
        margin: 20,
        nav: false,
        dots: false,
        loop: true,
        mouseDrag: false,
        navText: ['<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11 20"><path d="M10.8303 9.6013L1.00699 0.166606C0.7757 -0.0555352 0.404761 -0.0555352 0.173469 0.166606C-0.0578229 0.388746 -0.0578229 0.74501 0.173469 0.96715L9.57788 9.99948L0.173469 19.0318C-0.0578229 19.2539 -0.0578229 19.6102 0.173469 19.8323C0.286933 19.9413 0.439673 20 0.588048 20C0.736424 20 0.889164 19.9455 1.00263 19.8323L10.826 10.3977C11.0573 10.1797 11.0573 9.81925 10.8303 9.6013Z"/></svg>', '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11 20"><path d="M10.8303 9.6013L1.00699 0.166606C0.7757 -0.0555352 0.404761 -0.0555352 0.173469 0.166606C-0.0578229 0.388746 -0.0578229 0.74501 0.173469 0.96715L9.57788 9.99948L0.173469 19.0318C-0.0578229 19.2539 -0.0578229 19.6102 0.173469 19.8323C0.286933 19.9413 0.439673 20 0.588048 20C0.736424 20 0.889164 19.9455 1.00263 19.8323L10.826 10.3977C11.0573 10.1797 11.0573 9.81925 10.8303 9.6013Z"/></svg>'],
        responsive: {
          1023: {
            margin: 0,
            nav: true
          }
        }
      });
    }
  });
  $('body').on('click', '.b-product-card__dop-images-item', function () {
    productSlider.trigger('to.owl.carousel', [$(this).data('index'), 300]);
    $('.b-product-card__dop-images-item').removeClass('is-active');
    $('.b-product-card__dop-images-item[data-index="' + $(this).data('index') + '"]').addClass('is-active');
  });
  $('.b-product-card__params-all > a').on('click', function (e) {
    e.preventDefault();

    if ($('html, body').is(':animated')) {
      return false;
    }

    var href = $(this).attr('href');

    if ($(href).length) {
      var indent = parseInt($('.app-header__top-container').height());
      var k = false;
      $('html, body').animate({
        scrollTop: $(href).offset().top - indent
      }, 600, function () {
        setTimeout(function () {
          k = true;
        }, 100);
      });
      $('.b-card-tabs__titles-item').removeClass('active');
      $('.b-card-tabs__titles-item:first-child').addClass('active');
      $('.b-card-tabs__body').removeClass('active');
      $('#tabs1').addClass('active');
    }

    return false;
  }); // Rating

  var ratingProd = $('.b-product-card__rating-star').data('rating');
  $('.b-product-card__rating-star > span').css('width', ratingProd + '%');
  $('.b-reviews-tabs__rating-star').each(function () {
    var ratingProd = $(this).data('rating');
    $(this).find('> span').css('width', ratingProd + '%');
  });

  if (isMobile) {
    $('.b-product-card__big-image').lightGallery({
      selector: '.b-product-card__big-image a',
      thumbnail: false,
      download: false
    });
  } else {
    $('.b-product-card__big-image img').each(function () {
      $(this).imagezoomsl({
        zoomrange: [1, 10],
        cursorshadeborder: "2px solid #000",
        magnifiereffectanimate: "fadeIn",
        magnifiersize: [400, 300]
      });
    });
  } //---Адаптация карточки товара


  var prodDopImageAdaptationFlag = 1;

  function prodDopImageAdaptation() {
    if (window.matchMedia('(max-width:760px)').matches && prodDopImageAdaptationFlag == 1) {
      productThumbs.removeClass('owl-carousel').owlCarousel('destroy');
      prodDopImageAdaptationFlag = 2;
    } else if (window.matchMedia('(min-width:761px)').matches && prodDopImageAdaptationFlag == 2) {
      productThumbs.addClass('owl-carousel').owlCarousel({
        items: 4,
        margin: 20,
        nav: false,
        dots: false,
        loop: true,
        mouseDrag: false,
        navText: ['<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11 20"><path d="M10.8303 9.6013L1.00699 0.166606C0.7757 -0.0555352 0.404761 -0.0555352 0.173469 0.166606C-0.0578229 0.388746 -0.0578229 0.74501 0.173469 0.96715L9.57788 9.99948L0.173469 19.0318C-0.0578229 19.2539 -0.0578229 19.6102 0.173469 19.8323C0.286933 19.9413 0.439673 20 0.588048 20C0.736424 20 0.889164 19.9455 1.00263 19.8323L10.826 10.3977C11.0573 10.1797 11.0573 9.81925 10.8303 9.6013Z"/></svg>', '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11 20"><path d="M10.8303 9.6013L1.00699 0.166606C0.7757 -0.0555352 0.404761 -0.0555352 0.173469 0.166606C-0.0578229 0.388746 -0.0578229 0.74501 0.173469 0.96715L9.57788 9.99948L0.173469 19.0318C-0.0578229 19.2539 -0.0578229 19.6102 0.173469 19.8323C0.286933 19.9413 0.439673 20 0.588048 20C0.736424 20 0.889164 19.9455 1.00263 19.8323L10.826 10.3977C11.0573 10.1797 11.0573 9.81925 10.8303 9.6013Z"/></svg>'],
        responsive: {
          1023: {
            margin: 0,
            nav: true
          }
        }
      });
      prodDopImageAdaptationFlag = 1;
    }
  }

  $(window).on('resize', prodDopImageAdaptation).trigger('resize');
  var prodCardAdaptationFlag = 1;

  function prodCardAdaptation() {
    if (window.matchMedia('(max-width:1023px)').matches && prodCardAdaptationFlag == 1) {
      $('.b-product-card__name').prependTo('.b-product-card');
      prodCardAdaptationFlag = 2;
    } else if (window.matchMedia('(min-width:1024px)').matches && prodCardAdaptationFlag == 2) {
      $('.b-product-card__name').prependTo('.b-product-card__information');
      prodCardAdaptationFlag = 1;
    }
  }

  $(window).on('resize', prodCardAdaptation).trigger('resize');
  $('._js_add_to_compare').on('click tap', function (event) {
    event.preventDefault();
    $(this).closest('._js_product-item').toggleClass('selected');

    if ($(this).closest('._js_product-item').hasClass('selected') && window.matchMedia("(min-width: 1025px)").matches) {
      $('body').append('<div class="added-to-cart">Добавлено к сравению</div>');
      setTimeout(function () {
        $('.added-to-cart').fadeIn(200);
      }, 100);
      setTimeout(function () {
        $('.added-to-cart').fadeOut(function () {
          $('.added-to-cart').remove();
        });
      }, 2000);
    }

    if ($('._js_product-item.selected').length) {
      $('.compare-panel').addClass('show');
      $('.compare-panel__total-checked').text($('._js_product-item.selected').length);
      $('.app').addClass('show-compare-panel');
    } else {
      $('.compare-panel').removeClass('show');
      $('.app').removeClass('show-compare-panel');
    }

    $('.compare-panel__text').text(comparePanelText($('._js_product-item.selected').length));
  });

  function declOfNum(titles) {
    var number = Math.abs(number),
        cases = [2, 0, 1, 1, 1, 2];
    return function (number) {
      return titles[number % 100 > 4 && number % 100 < 20 ? 2 : cases[number % 10 < 5 ? number % 10 : 5]];
    };
  }

  ;
  var comparePanelText = declOfNum(['товар', 'товара', 'товаров']);
  $('._js_product_button').on('click tab', function (event) {
    $(this).closest('._js_product-item').addClass('in-cart');
    event.preventDefault();
    $('body').append('<div class="added-to-cart">Товар добавлен в корзину</div>');
    setTimeout(function () {
      $('.added-to-cart').fadeIn(200);
    }, 100);
    setTimeout(function () {
      $('.added-to-cart').fadeOut(function () {
        $('.added-to-cart').remove();
      });
    }, 2000);
  });
  $('.product-list.product-list--thumbs .product-item__image, .product-list--thumbs4 .product-item__image').equalHeightResponsive();
  $('.b-amount .b-amount__button').on('click tap', function (event) {
    event.preventDefault();
    var $this = $(this),
        amount = $this.closest('.b-amount').find('.b-amount__number'),
        min = amount.data('min'),
        value = parseInt(amount.val());

    if ($this.hasClass('minus')) {
      if (!$this.hasClass('disabled')) {
        if (value > min) {
          amount.val(value -= 1);
        } else if (value != 1) {
          $this.addClass('disabled'); // $(this).closest('._js_product-item').removeClass('in-cart');
        }

        if (value == min) {
          $this.addClass('disabled');

          if (!$this.hasClass('._js-amount')) {
            var errorMessage = $('<div class="b-amount__error"><span class="svg-icon svg-icon--error-icon"><svg class="svg-icon__link"><use xlink:href="#error-icon"></use></svg></span>Минимальная партия покупки: ' + min + ' шт</div>');
            errorMessage.appendTo($this.closest('.b-amount'));
          }

          setTimeout(function () {
            $('.b-amount__error').remove();
          }, 2000);
        }
      }
    } else {
      if (value >= min) {
        $this.closest('.b-amount').find('.minus').removeClass('disabled');
      }

      amount.val(value += 1); // $this.closest('.b-amount').find('.minus').removeClass('disabled')
    }
  });
  $('.b-amount .b-amount__number').keyup(function (e) {
    var min = $(this).data('min');

    if (this.value == "0" || this.value == "" || this.value < min) {
      this.value = min;
    } else {
      this.value = this.value.replace(/[^0-9\.]/g, '');
    }
  });
  $('.product-list').removeClass('hide');
  var prListFlag = 1;
  var thumbsListFlag = 1;

  if ($(window).width() <= 768) {
    $('.product-list.price-list').removeClass('product-list--price-list').addClass('product-list--simple');
    $('.product-list.thumbs').removeClass('product-list--thumbs').addClass('product-list--simple');
  }

  function priceList() {
    if ($('.product-list.price-list').length) {
      if ($(window).width() > 768 && prListFlag == 1) {
        $('.product-list.price-list .product-item__first, .product-list.price-list .product-item__middle, .product-list.price-list .product-item__last').unwrap();
        $('.product-list.price-list ._js_product-item').each(function () {
          var $this = $(this),
              first = $this.find('.product-item__first'),
              last = $this.find('.product-item__last'),
              code = $this.find('.product-item__code'),
              image = $this.find('.product-item__image'),
              inStock = $this.find('.product-item__in-stock'),
              price = $this.find('.product-item__price-container');
          code.appendTo(first);
          last.before(image);
          last.before(inStock);
          last.before(price);
        });
        $('.product-list.price-list').removeClass('product-list--simple').addClass('product-list--price-list');
        prListFlag = 2;
      } else if ($(window).width() <= 768 && prListFlag == 2) {
        $('.product-list.price-list ._js_product-item').each(function () {
          var $this = $(this),
              image = $this.find('.product-item__image'),
              first = $this.find('.product-item__first'),
              middle = $this.find('.product-item__middle'),
              middleTop = $this.find('.product-item__middle-top'),
              last = $this.find('.product-item__last'),
              labels = $this.find('.product-item__labels'),
              inStock = $this.find('.product-item__in-stock'),
              code = $this.find('.product-item__code'),
              price = $this.find('.product-item__price-container');
          $this.find('.product-item__first, .product-item__middle, .product-item__last').wrapAll('<div class="product-item__in"></div>');
          price.prependTo(last);
          inStock.prependTo(middleTop);
          code.prependTo(middleTop);
          labels.appendTo(first);
          image.appendTo(first);
        });
        $('.product-list.price-list').removeClass('product-list--price-list').addClass('product-list--simple');
        prListFlag = 1;
      }
    }
  }

  function simpleList() {
    if ($('.product-list.simple').length) {
      $('.product-list.simple ._js_product-item').each(function () {
        var $this = $(this),
            code = $this.find('.product-item__code'),
            middleTop = $this.find('.product-item__middle-top'),
            compare = $this.find('.product-item__compare'),
            last = $this.find('.product-item__last');
        code.prependTo(middleTop);
        compare.prependTo(last);
      });
    }
  }

  function thumbsList() {
    if ($('.product-list.thumbs').length) {
      if ($(window).width() <= 768 && thumbsListFlag == 1) {
        $('.product-list.thumbs ._js_product-item').each(function () {
          var $this = $(this),
              code = $this.find('.product-item__code'),
              middleTop = $this.find('.product-item__middle-top'),
              compare = $this.find('.product-item__compare'),
              last = $this.find('.product-item__last');
          code.prependTo(middleTop);
          compare.prependTo(last);
        });
        $('.product-list.thumbs').removeClass('product-list--thumbs').addClass('product-list--simple');
        thumbsListFlag = 2;
      } else if ($(window).width() > 768 && thumbsListFlag == 2) {
        $('.product-list.thumbs ._js_product-item').each(function () {
          var $this = $(this),
              code = $this.find('.product-item__code'),
              middleTop = $this.find('.product-item__middle-top'),
              compare = $this.find('.product-item__compare'),
              first = $this.find('.product-item__first');
          code.appendTo(first);
          compare.appendTo(first);
        });
        $('.product-list.thumbs').removeClass('product-list--simple').addClass('product-list--thumbs');
        thumbsListFlag = 1;
      }
    }
  }

  $(window).on('resize load', function () {
    priceList();
    simpleList();
    thumbsList();
  }); // if ($('.product-list.thumbs').length) {
  //     if($(window).width() <= 768) {
  //         $('.product-list.thumbs ._js_product-item').each(function(){
  //             var $this = $(this),
  //                 code = $this.find('.product-item__code'),
  //                 compare = $this.find('.product-item__compare'),
  //                 last = $this.find('.product-item__last'),
  //                 middleTop = $this.find('.product-item__middle-top');
  //                 compare.prependTo(last)
  //                 code.prependTo(middleTop)
  //         });    
  //         $('.product-list.thumbs')
  //             .removeClass('product-list--thumbs')
  //             .addClass('product-list--simple')
  //     } else {
  //         $('.product-list.thumbs ._js_product-item').each(function(){
  //             var $this = $(this),
  //                 code = $this.find('.product-item__code'),
  //                 compare = $this.find('.product-item__compare'),
  //                 first = $this.find('.product-item__first'),
  //                 middleTop = $this.find('.product-item__middle-top');
  //                 compare.prependTo(first)
  //                 code.prependTo(first)
  //         }); 
  //         $('.product-list.thumbs')
  //             .removeClass('product-list--simple')
  //             .addClass('product-list--thumbs')
  //     }
  // }
  // if ($('.product-list--simple.simple').length) {
  //     if($(window).width() <= 768) {
  //         $('.product-list.simple ._js_product-item').each(function(){
  //             var $this = $(this),
  //                 compare = $this.find('.product-item__compare'),
  //                 middleTop = $this.find('.product-item__middle-top'),
  //                 last = $this.find('.product-item__last'),
  //                 code = $this.find('.product-item__code');
  //                 code.prependTo(middleTop)
  //                 compare.appendTo(last)
  //         });    
  //     } else {
  //         $('.product-list.simple ._js_product-item').each(function(){
  //             var $this = $(this),
  //                 middleTop = $this.find('.product-item__middle-top'),
  //                 code = $this.find('.product-item__code'),
  //                 compare = $this.find('.product-item__compare'),
  //                 last = $this.find('.product-item__last');
  //                 compare.appendTo(last)
  //                 code.prependTo(middleTop)
  //         });    
  //     }
  // }

  $('.sort-by__title-container').on('click tap', function () {
    $(this).parent().toggleClass('opened');
  });
  $(document).on('click tap', '.mobile ._js_sort_item', function (event) {
    event.preventDefault();
    $('.sort-by__title').text($(this).text());
    $('.sort-by').removeClass('opened');
  });
  $('.b-sorting__items a').on('click tap', function () {
    $(this).toggleClass('is-active');
  });
  var cartonMapCoords = $('.cart-on-map__coords');
  var cartonMapList = $('.cart-on-map__list');
  $('.cart-on-map .two-way-switch').on('click', function () {
    if ($(this).find('[type="checkbox"]')[0].checked) {
      cartonMapCoords.stop().slideUp();
      cartonMapList.stop().slideDown();
      console.log('checked');
    } else {
      cartonMapCoords.stop().slideDown();
      cartonMapList.stop().slideUp();
    }
  });
  $('.document-scroll-icon span,.landing-banner__document-scroll-icon').on('click tap', function () {
    $('html, body').animate({
      scrollTop: $(window).height()
    }, 800);
  });

  function menuHeight() {
    if ($('.vertical.desctop').length) {
      $('.vertical.desctop > li > ul').css({
        'width': $(window).width() - 335 - $('.horisontal').offset().left
      });
    }

    var mh = 0;
    $('.app-header__folders-shared .vertical.desctop > li > ul').each(function () {
      var h_block = parseInt($(this).innerHeight());

      if (h_block > mh) {
        mh = h_block;
      }

      ;
    });
    $('.app-header__folders-shared .vertical.desctop > li > ul').height(mh - 16);
    $('.vertical.desctop').height(mh + 1);
  }

  var flag1023 = 1;
  var searcgResizeFlag = 1;
  var flag640 = 1;

  function siteResize() {
    if ($('.content-menu').length) {
      if (window.matchMedia('(max-width:640px)').matches && searcgResizeFlag == 1) {
        $('._js_search').addClass('mobile main').prependTo('.content-menu');
        searcgResizeFlag = 2;
      } else if (window.matchMedia('(min-width:641px)').matches && searcgResizeFlag == 2) {
        $('._js_search').removeClass('mobile main').appendTo('.app-header__middle');
        searcgResizeFlag = 1;
      }
    } else {
      if (window.matchMedia('(max-width:1023px)').matches && searcgResizeFlag == 1) {
        $('._js_search').addClass('mobile catalog').appendTo('.c-t-container');
        searcgResizeFlag = 2;
      } else if (window.matchMedia('(min-width:1024px)').matches && searcgResizeFlag == 2) {
        $('._js_search').removeClass('mobile catalog').appendTo('.app-header__middle');
        searcgResizeFlag = 1;
      }
    }

    if (window.matchMedia('(max-width:1023px)').matches && flag1023 == 1) {
      $('.app-header__cart').prependTo('.mobile-panel__content');
      $('.app-header__menu').appendTo('.mobile-panel__content');
      $('.app-header__folders-shared').appendTo('.mobile-panel__content');
      $('.sort-by').addClass('mobile').appendTo('.filter-sorting-wrapper'); // $('.b-filter').appendTo('.filter-sorting-wrapper__filter-container')   

      $('.b-filter').appendTo('body');
      flag1023 = 2;
    } else if (window.matchMedia('(min-width:1024px)').matches && flag1023 == 2) {
      $('.app-header__mobile-container').after($('.app-header__menu'));
      $('.app-header__cart').appendTo('.app-header__middle');
      $('.app-header__folders-shared').appendTo('.app-header__bot');
      $('.sort-by').removeClass('mobile').prependTo('.b-sorting__container');
      $('.b-filter').appendTo('.app__aside--right');
      flag1023 = 1;
    }

    menuHeight(); // if (window.matchMedia('(max-width:640px)').matches && flag640 == 1) {         
    //     $('.b-filter').appendTo('body')   
    //     flag640 = 2
    // } else if (window.matchMedia('(min-width:641px)').matches && flag640 == 2) {            
    //     $('.b-filter').appendTo('.filter-sorting-wrapper__filter-container')   
    //     flag640 = 1
    // }
  } // setTimeout(function(){
  //     $('html, body').animate({scrollTop: $('.b-breadcrumbs').offset().top}, 500)
  // }, 20)


  $(window).on('resize', siteResize).trigger('resize');
});