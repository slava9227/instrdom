$('.b-categor-lm__ul > li').each(function(){
	var $this = $(this);
	if ($this.find('> ul > li').length > 2) {
		var viewall = $('<li class="b-categor-lm__viewAll"><a href="#">Еще <span class="svg-icon svg-icon--arrow-b"><svg class="svg-icon__link"><use xlink:href="#arrow-b"></use></svg></span></a></li>'),
			ul = $this.find('> ul');

		viewall.appendTo(ul);
	}
});
$(document).on('click', '.b-categor-lm__viewAll > a', function(e) {
	e.preventDefault();

	$(this).toggleClass('opened').closest('ul').find('li').not('.b-categor-lm__viewAll, li:nth-child(1), li:nth-child(2)').slideToggle(400);
});