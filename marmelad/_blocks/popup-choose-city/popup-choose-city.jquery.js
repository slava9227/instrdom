var inst3 = $('[data-remodal-id=modal3]').remodal();
var inst4 = $('[data-remodal-id=modal4]').remodal();

$('.popup-choose-city__city').on('click tap', function(){
	$('.app-header__phone-sity > a').text($(this).text())	
	$('.app-header__phone-item > a').text($(this).data('phone'))	
	$('.app-header__phone-item > a').attr('href', 'tel:'+$(this).data('phone'))	

	inst3.close();
})

$('.chiose-city.remodal').parent().addClass('chiose-city-wrapper')
$('.choose-bottom.remodal').parent().addClass('choose-bottom-wrapper')

$('.popup-choose-city__form input').on('keyup', function() {
	if($(this).val() == "") {
		$('.popup-choose-city__form-dropdown-list').removeClass('show')		
	} else {
		$('.popup-choose-city__form-dropdown-list').addClass('show')
	}
});

$('.choose-bottom .type-8.remodal-cancel').on('click', function(){
	setTimeout(function() {
		inst3.open();
	}, 800);
})