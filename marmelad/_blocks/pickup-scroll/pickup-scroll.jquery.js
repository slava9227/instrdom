$('._js-scroll-to-block').on('click', function(event) {
	event.preventDefault();
	var scrollId       = $(this).data('scroll');
	var pageOffset     = $('.app-header__top-container').outerHeight();
	var scrollPosition = $('#' + scrollId).offset().top - pageOffset;
	$('body,html').stop().animate({
		scrollTop: scrollPosition
	},500,); 
});