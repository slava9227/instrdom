if($('.b-form--landing .b-form__item input[type=file]').length) {
	$('.b-form--landing .b-form__item input[type=file]').each(function(index, el) {
		var filePlaceholder =  $(this).data('placeholder');
		$(this).styler({
			filePlaceholder: filePlaceholder
		});		
	});
}