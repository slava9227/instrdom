$('.input_range_slider').each(function(index, el) {
    var $this = $(this),
        $lower = $this.closest('.range_slider_wrapper').find('.low'),
        $upper = $this.closest('.range_slider_wrapper').find('.hight'),
        inputRangeMax = $upper.data('val'),
        inputRangeMin = $lower.data('val'),
        placeHolder = $this.closest('.range_slider_wrapper').find('.b-filter__field input'),
        arr = [inputRangeMin, inputRangeMax];
        
    if ($(window).width() > 1023) {
        var slider = $this.noUiSlider({
            // start: [$lower.attr('value'), $upper.attr('value')],
            start: [inputRangeMin, inputRangeMax],
            connect: true,
            behaviour: 'drag-tap',
            format: wNumb({
              decimals: 0
            }),
            range: {
              'min': [ arr[0] ],
              'max': [ arr[1] ]
            }
          });

        slider.Link('lower').to($lower);
        slider.Link('upper').to($upper);

        $('.noUi-handle.noUi-handle-lower').append('<span class="s-from-span">'+ $lower.val().replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1 ') +'</span>')
        $('.noUi-handle.noUi-handle-upper').append('<span class="s-to-span">'+ $upper.val().replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1 ') +'</span>')

        slider.on('slide', function() {
            var newLoverValue = $lower.val(),
                newUpperValue = $upper.val();
                $this.find('.s-from-span').text($lower.val().replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1 '));
                $this.find('.s-to-span').text($upper.val().replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1 '));
        });

        slider.on('set.one', function() {
            var newLoverValue = $lower.val(),
                newUpperValue = $upper.val();
                $this.find('.s-from-span').text($lower.val().replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1 '));
                $this.find('.s-to-span').text($upper.val().replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1 '));
        });
    }       

    var inputVal = "";

    placeHolder.keyup(function(){
        $(this).addClass('tpp')
        if($('.tpp').length == 2) {
            $('.b-filter__reset-price').show()
        }
    })

    $('.b-filter__reset-price').on('click tap', function(){
        $lower.val(inputRangeMin)
        $upper.val(inputRangeMax)
        placeHolder.removeClass('tpp')
        $('.b-filter__reset-price').hide()
    })

    placeHolder.focus(function(){
        var $this = $(this);
        inputVal = $this.val()
        // $this.val('')

    });


    placeHolder.focusout(function(){
        var $this = $(this);

        if($this.val() == '') {
            $this.val(inputVal)  
        }
    });

});

$('.b-filter__item.toggle .b-filter__item-name').on('click tap', function(){
    if ($(window).width() > 1023){
        $(this).closest('.b-filter__item').find('.b-filter__item-params-list').slideToggle()
        $(this).closest('.b-filter__item').toggleClass('opened')
    }
});

$('.filter-button').on('click tap', function() {
    $('.b-filter').toggleClass('opened')
    documentAddClassOverflowHidden()
})

$('.b-filter__item-name').on('click tap', function(){
    var $this = $(this);
    if(!$this.closest('.b-filter__item').hasClass('b-filter__item--single')) {
        
        $this.closest('.b-filter__item-params-container').toggleClass('opened')
        $('.b-filter').toggleClass('no-scroll')        
    } 

});

function testParams() {
    if($('.b-filter__selected-container').children().length == 0) {
        $('.b-filter__sub-title').removeClass('show')
    } else {
        $('.b-filter__sub-title').addClass('show')        
    }
}

$(document).on('click tap', '._js_p-elem', function() {
    var $this = $(this),
        paramsContainer = $this.closest('.b-filter__item-params-container'),
        listIndex = paramsContainer.data('list-index'),
        itemIndex = $this.closest('.b-filter__item-param').data('item-index'),
        parentName = $this.closest('.b-filter__item-params-list').find('.b-filter__item-name').text(),
        name = $this.closest('.b-filter__item-param').find('span').text();

    paramsContainer.addClass('selected show-btn')   

    if($('.b-filter__selected-container .b-filter__checked-param').filter('[data-list-index="' + listIndex + '"]').length <= 0) {
        $('<div class="b-filter__checked-param"'+' data-list-index="'+listIndex+'"><span class="remove-item"></span><div class="parent-name">'+ parentName +'</div><div class="items-container" data-list-index="'+listIndex+'"></div></div>').appendTo('.b-filter__selected-container')
    }

    if ($this.hasClass('checked') && !$this.closest('.b-filter__item-param').hasClass('cloned') && !$this.closest('.b-filter__item').hasClass('b-filter__item--single')) {
        $('.b-filter__checked-param')
            .filter('[data-list-index="' + listIndex + '"]')
            .find('.items-container')
            .append('<span class="item-name" data-item-index="'+itemIndex+'">'+name+'</span>')
    } else {
        $('.b-filter__checked-param')
            .filter('[data-list-index="' + listIndex + '"]')
            .find('.item-name')
            .filter('[data-item-index="' + itemIndex + '"]')
            .remove()    
    }    

    if($('.b-filter__checked-param').filter('[data-list-index="' + listIndex + '"]').find('.items-container').children('.item-name').length == 0) {
       $('.b-filter__checked-param')
        .filter('[data-list-index="' + listIndex + '"]')
        .remove()
        paramsContainer.removeClass('selected')
    }

    testParams()
    $('.b-filter').addClass('show-submit-btn')

});

$(document).on('click tap', '.b-filter__item-param.cloned ._js_p-elem', function(){
    $(this).toggleClass('checked')
    $(this).parent().toggleClass('checked')
})

$(document).on('click', '.items-container', function(){
    var $this = $(this),
        listIndex = $this.data('list-index');
        $('.b-filter__item-params-container')
            .filter('[data-list-index="' + listIndex + '"]')
            .addClass('opened')
            .removeClass('show-btn')
})

$('.b-filter__back-btn, .b-filter__item-name--mobile').on('click tap', function(){
    var $this = $(this),
        container = $this.closest('.b-filter__item-params-list').find('.b-filter__for-checked-items-container'),
        itemsContainer = container.find('.b-filter__selected-items-container');


    setTimeout(function(){

        $('.b-filter__selected-items-container .b-filter__item-param').each(function(){
            if (!$(this).hasClass('checked')) {
                $(this).remove()
            }
        });        
        container.addClass('show')   
        $('.b-filter__list-box .b-filter__item-param.checked') 
            .not(':hidden')             
            .hide()
            .clone(true)
            .show()
            .appendTo(itemsContainer)
            .addClass('cloned')  

        $('.b-filter__item-param').not('.checked').show()

        if(container.find('.b-filter__selected-items-container').children().length == 0) {
             container.removeClass('show')  
        } else {
             container.addClass('show')          
        }

        showTotalParams();

    }, 400)

    $this.closest('.b-filter__item-params-container').removeClass('opened')

})


$(document).on('click tap', '.remove-item', function(){
    var $this = $(this),
        listIndex = $this.closest('.b-filter__checked-param').data('list-index');

    if($this.closest('.b-filter__checked-param').hasClass('single')) {

        $this.closest('.b-filter__checked-param')

        $this
            .closest('.b-filter__checked-param')
            .find('.b-filter__item-param')
            .appendTo($('.b-filter__item-params-container').filter('[data-list-index="' + listIndex + '"]'))
            .find('._js_p-elem-single').removeClass('checked').prop('checked', false)

        $('.b-filter__item-params-container').filter('[data-list-index="' + listIndex + '"]').closest('.b-filter__item').show()

        $('.b-filter__checked-param').filter('[data-list-index="' + listIndex + '"]').remove()          

    } else {              

        $this.closest('.b-filter__checked-param').remove()

        $('.b-filter__item-params-container')
            .filter('[data-list-index="' + listIndex + '"]')
            .removeClass('selected')
            .find('.b-filter__for-checked-items-container')
            .removeClass('show')
            .children('.b-filter__item-param')
            .remove()
            
        $('.b-filter__item-params-container')
            .filter('[data-list-index="' + listIndex + '"]')
            .find('.b-filter__item-param')
            .removeClass('checked')
            .show()
            .find('._js_p-elem')
            .removeClass('checked')
            .find('input')
            .prop('checked', false)            
    }     

    testParams()   
    $('.b-filter').addClass('show-submit-btn')
})

$('.b-filter__items-reset').on('click tap', function(){
    var $this = $(this),
        container = $this.closest('.b-filter__item-params-container'),
        listIndex = container.data('list-index');

    container.removeClass('selected').addClass('show-btn')

    container
        .find('.b-filter__for-checked-items-container')
        .removeClass('show')

    container
        .find('.b-filter__selected-items-container')
        .children()
        .remove()

    container
        .find('.b-filter__item-param')
        .removeClass('checked')
        .show()
        .find('._js_p-elem')
        .removeClass('checked')
        .find('input')
        .prop('checked', false)    

    $('.b-filter__selected-container .b-filter__checked-param').filter('[data-list-index="' + listIndex + '"]').remove()

    testParams()

    $('.b-filter').addClass('show-submit-btn')
});

$('.reset-all').on('click tap', function(){

    $('.b-filter__selected-container ._js_p-elem-single').each(function(){
        var $this = $(this),
            listIndex = $this.closest('.b-filter__checked-param').data('list-index');

            $this.removeClass('checked').prop('checked', false)

            $this.closest('.b-filter__item-param').appendTo($('.b-filter__item-params-container').filter('[data-list-index="' + listIndex + '"]'))

        // $this.closest('.b-filter__item-param').appendTo($('.b-filter__item-params-container').filter('[data-list-index="' + listIndex + '"]'))
        
    });

    $('.b-filter__item--single').show()

    $('.b-filter__checked-param').remove()
    $('.b-filter__sub-title').removeClass('show')
    $('.b-filter__item-params-container').removeClass('selected show-btn')
    $('.b-filter__for-checked-items-container')
        .removeClass('show')
        .find('.b-filter__item-param').remove()

    $('.b-filter__item-param')
        .removeClass('checked')
        .show()    
        .find('._js_p-elem')
        .removeClass('checked')
        .find('input')
        .prop('checked', false)       

    $('.b-filter').addClass('show-submit-btn')     
});

function checkedParams() {
    $('._js_p-elem-single').each(function(){
        var $this = $(this);

        if($this.is(':checked')) { 
            var name = $this.closest('.b-filter__item').find('.b-filter__item-name').text(),
                listIndex = $this.closest('.b-filter__item-params-container').data('list-index'),
                param = $this.closest('.b-filter__item-param');

            $this.closest('.b-filter__item--single').hide()

            $('<div class="b-filter__checked-param checked single"'+' data-list-index="'+listIndex+'"><span class="remove-item"></span><div class="parent-name">'+ name +'</div></div>').appendTo('.b-filter__selected-container')    
            

            if ($('.b-filter__checked-param').filter('[data-list-index="' + listIndex + '"]').children('.b-filter__item-param').length <= 0) {
                param.addClass('single').appendTo($('.b-filter__checked-param').filter('[data-list-index="' + listIndex + '"]'))                
            }

            testParams()

        } else {
            var listIndex = $this.closest('.b-filter__checked-param').data('list-index');

            $this.closest('.b-filter__item-param').appendTo($('.b-filter__item-params-container').filter('[data-list-index="' + listIndex + '"]'))

            $('.b-filter__item-params-container').filter('[data-list-index="' + listIndex + '"]').closest('.b-filter__item').show()

            $('.b-filter__checked-param').filter('[data-list-index="' + listIndex + '"]').remove()

            testParams()           
        }

    });
}

checkedParams()

$(document).on('click tap', '._js_p-elem-single', function(){
    $('.b-filter').addClass('show-submit-btn')
})

$('.b-filter__button').on('click tap', function(evet){
    evet.preventDefault();
    $('.b-filter').removeClass('opened show-submit-btn')
    $('html').removeClass('overflow-hidden')
    checkedParams()
    showTotalParams()
})

function showTotalParams() {

    if(($('.b-filter__checked-param.checked.single').length + $('.b-filter__list-box .b-filter__item-param.checked').length) > 0) {
        $('.pr-total').addClass('show')
    } else {
        $('.pr-total').removeClass('show')        
    }

    $('.pr-total').text(($('.b-filter__checked-param.checked.single').length + $('.b-filter__list-box .b-filter__item-param.checked').length))
   
}

showTotalParams()
