$('.metro-select__button').on('click', function() {
  $(this).closest('.metro-select').toggleClass('is-opened');
});

$('.metro-name').on('click', function() {
  let $this = $(this);
  let $select = $this.closest('.metro-select');

  $this.addClass('metro-name--checked').siblings().removeClass('metro-name--checked');

  $select
    .toggleClass('is-opened')
    .find('.metro-select__label div').text($this.text());
});


$(document).on('click', function(event) {

  if ($(event.target).closest('.metro-select').length < 1) {
    $('.metro-select').removeClass('is-opened');
  }

});
