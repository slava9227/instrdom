$('.sort-by__title-container').on('click tap', function(){
	$(this).parent().toggleClass('opened')
});

$(document).on('click tap', '.mobile ._js_sort_item', function(event){
	event.preventDefault();
	$('.sort-by__title').text($(this).text())
	$('.sort-by').removeClass('opened')
});

$('.b-sorting__items a').on('click tap', function(){
	$(this).toggleClass('is-active');
});