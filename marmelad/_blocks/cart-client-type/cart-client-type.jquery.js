let cientTypeRadios = $('.cart-client-type [type="radio"]');
let lawyerFields = $('.lawyer-fields');


cientTypeRadios.on('change', function() {

  if (cientTypeRadios.filter(":checked").val() == 2) {
    lawyerFields.stop().slideDown();
  } else {
    lawyerFields.stop().slideUp();
  }
});
