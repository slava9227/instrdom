var productSlider = $('.b-product-card__big-image'),
	productItems  = productSlider.find('.b-product-card__big-image-item'),
	productThumbs = $('.b-product-card__dop-images'),
	isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || /[\?&]panel_fake_mobile=1(&|$)/.test(document.location.search);

	$.each(productItems, function(i, el){
	  var classActive = (i == 0) ? 'is-active' : ''; 
	  var item = $('<div class="b-product-card__dop-images-item ' + classActive + '" data-index="' + i + '"><img src="' + $(el).data('thumb') + '" alt=""></div>');

	  item.appendTo(productThumbs);
	});

  productSlider.addClass('owl-carousel').owlCarousel({
	  mouseDrag: false,
	  items: 1,
	  animateOut: 'fadeOut',
	  nav     : false,
	  onChanged : function(event) {
	  	var $thisIndex = event.item.index;
	  	$('.b-product-card__dop-images-item').removeClass('is-active');
		$('.b-product-card__dop-images-item[data-index="' + $thisIndex + '"]').addClass('is-active');
	  },
	onInitialize: function (event) {
		productThumbs.addClass('owl-carousel').owlCarousel({
		  items    : 4,
		  margin   : 20,
		  nav      : false,
		  dots: false,
		  loop: true,
		  mouseDrag: false,
		  navText  : ['<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11 20"><path d="M10.8303 9.6013L1.00699 0.166606C0.7757 -0.0555352 0.404761 -0.0555352 0.173469 0.166606C-0.0578229 0.388746 -0.0578229 0.74501 0.173469 0.96715L9.57788 9.99948L0.173469 19.0318C-0.0578229 19.2539 -0.0578229 19.6102 0.173469 19.8323C0.286933 19.9413 0.439673 20 0.588048 20C0.736424 20 0.889164 19.9455 1.00263 19.8323L10.826 10.3977C11.0573 10.1797 11.0573 9.81925 10.8303 9.6013Z"/></svg>','<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11 20"><path d="M10.8303 9.6013L1.00699 0.166606C0.7757 -0.0555352 0.404761 -0.0555352 0.173469 0.166606C-0.0578229 0.388746 -0.0578229 0.74501 0.173469 0.96715L9.57788 9.99948L0.173469 19.0318C-0.0578229 19.2539 -0.0578229 19.6102 0.173469 19.8323C0.286933 19.9413 0.439673 20 0.588048 20C0.736424 20 0.889164 19.9455 1.00263 19.8323L10.826 10.3977C11.0573 10.1797 11.0573 9.81925 10.8303 9.6013Z"/></svg>'],
		  responsive : {
		    	1023: {
				  margin   : 0,
				  nav      : true
		    	}
		    }
		});
	}
  });



$('body').on('click', '.b-product-card__dop-images-item', function(){
	productSlider.trigger('to.owl.carousel', [$(this).data('index'), 300]);
	$('.b-product-card__dop-images-item').removeClass('is-active');
	$('.b-product-card__dop-images-item[data-index="' + $(this).data('index') + '"]').addClass('is-active');
});

$('.b-product-card__params-all > a').on('click', function(e){
	e.preventDefault();

    if ($('html, body').is(':animated')) {
    	return false;
    }

    var href = $(this).attr('href');

    if ($(href).length) {
        var indent = parseInt($('.app-header__top-container').height());
        var k = false;

        $('html, body').animate({
            scrollTop: $(href).offset().top - indent
        }, 600, function () {
            setTimeout(function () {
                k = true;
            }, 100);
        });
		$('.b-card-tabs__titles-item').removeClass('active');
		$('.b-card-tabs__titles-item:first-child').addClass('active');

		$('.b-card-tabs__body').removeClass('active');
		$('#tabs1').addClass('active');
    }
    return false;
});

// Rating
var ratingProd = $('.b-product-card__rating-star').data('rating');

$('.b-product-card__rating-star > span').css('width', ratingProd+'%' );

$('.b-reviews-tabs__rating-star').each(function(){
	var ratingProd = $(this).data('rating');

	$(this).find('> span').css('width', ratingProd+'%' );
});


if (isMobile) {
	$('.b-product-card__big-image').lightGallery({
        selector: '.b-product-card__big-image a',
        thumbnail: false,
        download: false
	});
} else {
	$('.b-product-card__big-image img').each(function(){
		$(this).imagezoomsl({
	       zoomrange: [1, 10],
	       cursorshadeborder: "2px solid #000",
	       magnifiereffectanimate: "fadeIn",
	       magnifiersize: [400, 300]
		});
	});
}
//---Адаптация карточки товара
var prodDopImageAdaptationFlag = 1;
function prodDopImageAdaptation() {
	if (window.matchMedia('(max-width:760px)').matches && prodDopImageAdaptationFlag == 1) {
		
		productThumbs.removeClass('owl-carousel').owlCarousel('destroy');

		prodDopImageAdaptationFlag = 2;
	} else if (window.matchMedia('(min-width:761px)').matches && prodDopImageAdaptationFlag == 2) {
		
		productThumbs.addClass('owl-carousel').owlCarousel({
		  items    : 4,
		  margin   : 20,
		  nav      : false,
		  dots: false,
		  loop: true,
		  mouseDrag: false,
		  navText  : ['<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11 20"><path d="M10.8303 9.6013L1.00699 0.166606C0.7757 -0.0555352 0.404761 -0.0555352 0.173469 0.166606C-0.0578229 0.388746 -0.0578229 0.74501 0.173469 0.96715L9.57788 9.99948L0.173469 19.0318C-0.0578229 19.2539 -0.0578229 19.6102 0.173469 19.8323C0.286933 19.9413 0.439673 20 0.588048 20C0.736424 20 0.889164 19.9455 1.00263 19.8323L10.826 10.3977C11.0573 10.1797 11.0573 9.81925 10.8303 9.6013Z"/></svg>','<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 11 20"><path d="M10.8303 9.6013L1.00699 0.166606C0.7757 -0.0555352 0.404761 -0.0555352 0.173469 0.166606C-0.0578229 0.388746 -0.0578229 0.74501 0.173469 0.96715L9.57788 9.99948L0.173469 19.0318C-0.0578229 19.2539 -0.0578229 19.6102 0.173469 19.8323C0.286933 19.9413 0.439673 20 0.588048 20C0.736424 20 0.889164 19.9455 1.00263 19.8323L10.826 10.3977C11.0573 10.1797 11.0573 9.81925 10.8303 9.6013Z"/></svg>'],
		  responsive : {
		    	1023: {
				  margin   : 0,
				  nav      : true
		    	}
		    }
		});

		prodDopImageAdaptationFlag = 1;
	}
}
$(window).on('resize', prodDopImageAdaptation).trigger('resize');

var prodCardAdaptationFlag = 1;

function prodCardAdaptation() {
	if (window.matchMedia('(max-width:1023px)').matches && prodCardAdaptationFlag == 1) {
		$('.b-product-card__name').prependTo('.b-product-card');

		prodCardAdaptationFlag = 2;
	} else if (window.matchMedia('(min-width:1024px)').matches && prodCardAdaptationFlag == 2) {
		$('.b-product-card__name').prependTo('.b-product-card__information');

		prodCardAdaptationFlag = 1;
	}
}
$(window).on('resize', prodCardAdaptation).trigger('resize');