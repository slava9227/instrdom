function documentAddClassOverflowHidden() {
	if (/iPod|iPad|iPhone/i.test(navigator.userAgent)) {
		$('body').addClass('overflow-hidden');
	} else {
		$('body').removeClass('overflow-hidden');
		$(document.documentElement).addClass('overflow-hidden');
	}
}

function documentRemoveClassOverflowHidden() {
	$('body').removeClass('overflow-hidden');
	$(document.documentElement).removeClass('overflow-hidden');
}

;(function(){


	$('._js_search span').on('click tap', function(){
		$(this).closest('._js_search').toggleClass('opened')
		$(this).closest('._js_search').find('input').focus()
	})

	$('._js_search.mobile input').on('keypress', function(){
		var $this = $(this),
			letLength = $this.val();
		$this.closest('._js_search.mobile').addClass('typing')

		if ($this.val().length < 0 ) {
			$('._js_search.mobile').removeClass('typing')
		}
	})

	$('._js_search').submit(function(event){
		if($(this).find('input').val() == '') {
			event.preventDefault();
			$(this).removeClass('opened')
		}
	});

	$('._js_search.mobile input').on('keyup', function(){
		var $this = $(this),
			letLength = $this.val();
		if ($this.val().length == 0 ) {
			$('._js_search.mobile').removeClass('typing')
		}
	})

	$('.mobile-panel').css({
	    'height': $(window).height() + 60
	})

	$('.app-header__burger, .landing-header__burger').on('click tap', function(){
		$('.mobile-panel').addClass('opened')
		$('.app__overlay').addClass('opened')
		documentAddClassOverflowHidden();
	});

	$('.mobile-panel__close').on('click tap', function(){
		$('.mobile-panel').removeClass('opened')
		$('.app__overlay').removeClass('opened')
		documentRemoveClassOverflowHidden()
	});

	$(document).on('click tap', function(event){
	    if ($(event.target).closest('.app-header__burger, .landing-header__burger, .mobile-panel__content, .toggle-catalog, .vertical, .folders-list__item, .search-form__container, .content-menu .has-levels, .b-filter, .filter-button, .b-filter__checked-param').length) {
	    	return;
	    }
	    $('.mobile-panel').removeClass('opened')
	    $('.app__overlay').removeClass('opened')
	    $('.toggle-catalog').removeClass('is-active')
	    $('.vertical').removeClass('opened')
	    $('.folders-list__item').removeClass('opened')
	    $('._js_search').removeClass('opened')
	    $('.b-filter').removeClass('opened')
		documentRemoveClassOverflowHidden();
	});

	$('.app-header__folders-shared .has-sublevels').on({
		mouseover: function() {
			var $this = $(this),
				index = $this.data('index');

			$('.horisontal .has-sublevels').filter('[data-index="' + index + '"]').addClass('is-active')
			$('.toggle-catalog').addClass('opened')
		},
		mouseleave : function(){
			var $this = $(this),
				index = $this.data('index');
				$('.horisontal .has-sublevels').filter('[data-index="' + index + '"]').removeClass('is-active')
				$('.toggle-catalog').removeClass('opened')
		}
	})

	$('.vertical li').each(function(){
		var $this = $(this);
		if($this.children('ul').length) {
			$this.addClass('has-sublevels')
			$this.children('a').append('<span class="arrow"><svg width="6" height="8" viewBox="0 0 6 8" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M0.734375 0.933333L1.66771 0L5.66771 4L1.66771 8L0.734375 7.06667L3.80104 4L0.734375 0.933333Z" fill="white"/></svg></span>')
		}
	})

	$('.svg-icon--arrow-b').on('click tab', function(){
		$('.search-block__sorting').toggleClass('opened')
	})

	function selectText(elementId) {
		var doc = document,
		text = doc.getElementById(elementId),
		range,
		selection;
		if (doc.body.createTextRange) {
			range = document.body.createTextRange();
			range.moveToElementText(text);
			range.select();
		} else if (window.getSelection) {
			selection = window.getSelection();
			range = document.createRange();
			range.selectNodeContents(text);
			selection.removeAllRanges();
			selection.addRange(range);
		}
	}

	$(".copy").click(function(e) {
		e.preventDefault();
		selectText(this.id);
		document.execCommand("copy");
	});

	$('.vertical.mobile li ul').css({
		'height': $(window).height()
	})

	$('.vertical.mobile li').each(function(){
		var $this = $(this),
			subLevels = $this.find('>ul');

		if(subLevels.length > 0) {
			$this.find('>a').clone().addClass('back').prependTo(subLevels).wrap('<li>')
		}

	})

	$('.vertical.mobile > .has-sublevels > a').on('click tap', function(event){
		event.preventDefault();
		$(this).parent().addClass('is-opened')
		$(this).parent().find('>ul').addClass('opened')
	});

	$('.back').on('click tap', function(event){
		event.preventDefault();
		$(this).closest('ul').removeClass('opened')
		$(this).closest('.has-sublevels').removeClass('is-opened')

	})

	var docSrollFlag = 1;

	// $(document).scroll(function(){
	// 	if($(window).width() > 1023) {
	// 		if($(document).scrollTop() > 0 && docSrollFlag == 1) {
	// 			$('.app-header').addClass('scroll')
	// 			docSrollFlag = 2
	// 		} else if($(document).scrollTop() <= 0 && docSrollFlag == 2) {
	// 			$('.app-header').removeClass('scroll')
	// 			docSrollFlag = 1
	// 		}
	// 	}
	// })

	$(document).scroll(function() {
		if($(window).width() > 1023) {
			if($(document).scrollTop() > ($('.app-header__top-container').height() + $('.app-header__bot').height()) && docSrollFlag == 1) {
				$('.app-header').addClass('scroll')

				$('.app-header__logo').appendTo('.app-header__fc-l')	
				$('.toggle-catalog').appendTo('.app-header__fc-l')	
				$('.app-header__middle .search-form._js_search').appendTo('.app-header__fc-l')	
				$('.app-header__button.s-button.type-2').appendTo('.app-header__fc-l')	
				$('.app-header__e-mail').appendTo('.app-header__fixed-container-in')	
				$('.app-header__phone-item').appendTo('.app-header__fixed-container-in')	
				$('.app-header__cart').appendTo('.app-header__fixed-container-in')	

				// $('.has-sublevels ul').height($(window).height() - 89)	

				$('.vertical.desctop').addClass('fix')

				if ( ($('.vertical.desctop.fix').innerHeight() -9) > $(window).height()) {
					$('.vertical.desctop.fix').css({
						'height': $(window).height() - 89
					})
				}	

				if( ($('.vertical.desctop.fix > .has-sublevels > ul').innerHeight() - 9) > $(window).height())	{
					$('.vertical.desctop.fix > .has-sublevels > ul').css({
						'height': $(window).height() - 89,
						'overflow-y': 'auto'
					})
				}
				
				$('.vertical.desctop.fix > .has-sublevels > ul').css({
					'top': 89,
					'left': $('.vertical.desctop.fix').offset().left + 335
				})

				docSrollFlag = 2
			} else if($(document).scrollTop() <= ($('.app-header__top-container').height() + $('.app-header__bot').height()) && docSrollFlag == 2) {
				$('.app-header').removeClass('scroll')

				$('.app-header__logo').prependTo('.app-header__middle')
				$('.toggle-catalog').prependTo('.horisontal')
				$('.app-header__middle .search-form._js_search').appendTo('.app-header__fixed-container-in')
				$('.app-header__menu').after($('.app-header__e-mail'))
				$('.app-header__phone-item').appendTo('.app-header__phone')
				$('.app-header__button.s-button.type-2').appendTo('.app-header__middle')
				$('.search-form._js_search').appendTo('.app-header__middle')
				$('.app-header__cart').appendTo('.app-header__middle')
				$('.vertical.desctop').removeClass('fix')	
				$('.vertical.desctop .has-sublevels > ul').css({
					'top': 0,
					'left': 100 + '%',
					'overflow-y': 'visible'
				})

				docSrollFlag = 1
			}
		}
	})

})();

