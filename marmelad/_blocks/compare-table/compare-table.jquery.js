// let compareTableProducts = $('.compare-table__products');
// let compareFeatures = $('.compare-features');

// compareFeatures.css({
//   paddingBottom: $(compareTableProducts).outerHeight() - compareProductsList.height();
// });

let compareProductsList = $('.compare-products-list');

compareProductsList
  .addClass('owl-carousel')
  .owlCarousel({
    items: 4,
    margin: 0,
    dots: false,
    nav: true,
    responsive: {
      0: {
        items: 1
      },
      540: {
        items: 2
      },
      720: {
        items: 3
      },
      1140: {
        items: 4
      }
    }
  });
