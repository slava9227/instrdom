$('.b-card-tabs__titles-item > a').on('click', function(e){
	e.preventDefault();

	var $this = $(this),
		li = $this.closest('li'),
		id = $this.attr('href');

	$('.b-card-tabs__titles-item').removeClass('active');
	li.addClass('active');

	$('.b-card-tabs__body').removeClass('active');
	$(id).addClass('active');
});
$('.b-card-tabs__body-name').on('click', function(e){
	e.preventDefault();

	var $this = $(this),
		id = $this.closest('.b-card-tabs__body'),
		li = $('.b-card-tabs__titles-item:eq('+id.index()+')');

	if (id.hasClass('active')) {
		$('.b-card-tabs__titles-item').removeClass('active');
		$('.b-card-tabs__body').removeClass('active');
	} else {
		$('.b-card-tabs__titles-item').removeClass('active');
		li.addClass('active');

		$('.b-card-tabs__body').removeClass('active');
		id.addClass('active');
	}

    var indent = parseInt($('.app-header__top').outerHeight());
    
    $('html, body').animate({
        scrollTop: $this.offset().top - indent
    }, 600);
});
