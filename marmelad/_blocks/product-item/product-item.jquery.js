$('._js_add_to_compare').on('click tap', function(event) {
	event.preventDefault();
	$(this).closest('._js_product-item').toggleClass('selected')

    if($(this).closest('._js_product-item').hasClass('selected') && window.matchMedia("(min-width: 1025px)").matches) {
        $('body').append('<div class="added-to-cart">Добавлено к сравению</div>');
        setTimeout(function () {
            $('.added-to-cart').fadeIn(200);
        }, 100);
        setTimeout(function () {
            $('.added-to-cart').fadeOut(function(){
                $('.added-to-cart').remove()
            });
        }, 2000);
    }

    if($('._js_product-item.selected').length) {
        $('.compare-panel').addClass('show')
        $('.compare-panel__total-checked').text($('._js_product-item.selected').length)
        $('.app').addClass('show-compare-panel')
    } else {
        $('.compare-panel').removeClass('show')
        $('.app').removeClass('show-compare-panel')
    }

    $('.compare-panel__text').text(comparePanelText($('._js_product-item.selected').length))

});

function declOfNum(titles) {
    var number = Math.abs(number),
        cases = [2, 0, 1, 1, 1, 2];
    return function (number) {
        return titles[number % 100 > 4 && number % 100 < 20 ? 2 : cases[number % 10 < 5 ? number % 10 : 5]];
    };
};

var comparePanelText = declOfNum(['товар', 'товара', 'товаров']);

$('._js_product_button').on('click tab', function (event) {

    $(this).closest('._js_product-item').addClass('in-cart')

	event.preventDefault();
    $('body').append('<div class="added-to-cart">Товар добавлен в корзину</div>');

    setTimeout(function () {
        $('.added-to-cart').fadeIn(200);
    }, 100);
    setTimeout(function () {
        $('.added-to-cart').fadeOut(function(){
        	$('.added-to-cart').remove()
        });
    }, 2000);
});

$('.product-list.product-list--thumbs .product-item__image, .product-list--thumbs4 .product-item__image').equalHeightResponsive();

$('.b-amount .b-amount__button').on('click tap', function(event){
    event.preventDefault();
    var $this = $(this),
        amount = $this.closest('.b-amount').find('.b-amount__number'),
        min = amount.data('min'), 
        value = parseInt(amount.val());

    if($this.hasClass('minus')) {
        if(!$this.hasClass('disabled')) {

            if (value > min) {
                amount.val(value -= 1);

            } else if (value !=1) {
                $this.addClass('disabled');
                // $(this).closest('._js_product-item').removeClass('in-cart');

            }        

            if (value == min) {
                $this.addClass('disabled');

                if (!$this.hasClass('._js-amount')) {

                    var errorMessage = $('<div class="b-amount__error"><span class="svg-icon svg-icon--error-icon"><svg class="svg-icon__link"><use xlink:href="#error-icon"></use></svg></span>Минимальная партия покупки: '+ min +' шт</div>')
                    errorMessage.appendTo($this.closest('.b-amount'));
                }

                setTimeout(function(){
                    $('.b-amount__error').remove();
                }, 2000);
            }      
        }    
    } else {
        if (value >= min) {
            $this.closest('.b-amount').find('.minus').removeClass('disabled');
        }
        amount.val(value += 1);
        // $this.closest('.b-amount').find('.minus').removeClass('disabled')
    }    

})

$('.b-amount .b-amount__number').keyup(function(e){
    var min = $(this).data('min');
    
    if(this.value == "0" || this.value == "" || this.value < min) {
        this.value = min;
    } else {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    }
});

$('.product-list').removeClass('hide');

var prListFlag = 1;
var thumbsListFlag = 1;

if($(window).width() <= 768) {
    $('.product-list.price-list')
        .removeClass('product-list--price-list')
        .addClass('product-list--simple')

    $('.product-list.thumbs')
        .removeClass('product-list--thumbs')
        .addClass('product-list--simple')
}

function priceList() {
    if ($('.product-list.price-list').length) {
        if($(window).width() > 768 && prListFlag == 1) {

            $('.product-list.price-list .product-item__first, .product-list.price-list .product-item__middle, .product-list.price-list .product-item__last').unwrap()

            $('.product-list.price-list ._js_product-item').each(function() {
                var $this = $(this),
                    first = $this.find('.product-item__first'),
                    last = $this.find('.product-item__last'),
                    code = $this.find('.product-item__code'),
                    image = $this.find('.product-item__image'),
                    inStock = $this.find('.product-item__in-stock'),
                    price = $this.find('.product-item__price-container');

                    code.appendTo(first)
                    last.before(image)
                    last.before(inStock)
                    last.before(price)
            })

            $('.product-list.price-list')
                .removeClass('product-list--simple')
                .addClass('product-list--price-list')
                
            prListFlag = 2 
        } else if($(window).width() <= 768 && prListFlag == 2) {

            $('.product-list.price-list ._js_product-item').each(function(){
                var $this = $(this),
                    image = $this.find('.product-item__image'),
                    first = $this.find('.product-item__first'),
                    middle = $this.find('.product-item__middle'),
                    middleTop = $this.find('.product-item__middle-top'),
                    last = $this.find('.product-item__last'),
                    labels = $this.find('.product-item__labels'),
                    inStock = $this.find('.product-item__in-stock'),
                    code = $this.find('.product-item__code'),
                    price = $this.find('.product-item__price-container');

                    $this.find('.product-item__first, .product-item__middle, .product-item__last').wrapAll('<div class="product-item__in"></div>')

                    price.prependTo(last)
                    inStock.prependTo(middleTop)
                    code.prependTo(middleTop)
                    labels.appendTo(first)
                    image.appendTo(first)

            })

            $('.product-list.price-list')
                .removeClass('product-list--price-list')
                .addClass('product-list--simple')
            prListFlag = 1
        } 
    }
}

function simpleList() {
    if ($('.product-list.simple').length) {
        $('.product-list.simple ._js_product-item').each(function(){
            var $this = $(this),
                code = $this.find('.product-item__code'),                
                middleTop = $this.find('.product-item__middle-top'),
                compare = $this.find('.product-item__compare'),
                last = $this.find('.product-item__last');

                code.prependTo(middleTop)
                compare.prependTo(last)
        })
    }
}

function thumbsList() {
    if ($('.product-list.thumbs').length) {
        if($(window).width() <= 768 && thumbsListFlag == 1) {
            $('.product-list.thumbs ._js_product-item').each(function(){
                var $this = $(this),
                    code = $this.find('.product-item__code'),                
                    middleTop = $this.find('.product-item__middle-top'),
                    compare = $this.find('.product-item__compare'),
                    last = $this.find('.product-item__last');

                    code.prependTo(middleTop)
                    compare.prependTo(last)
        })

        $('.product-list.thumbs')
            .removeClass('product-list--thumbs')
            .addClass('product-list--simple')

            thumbsListFlag = 2
        } else if($(window).width() > 768 && thumbsListFlag == 2) {
            $('.product-list.thumbs ._js_product-item').each(function(){
                var $this = $(this),
                    code = $this.find('.product-item__code'),                
                    middleTop = $this.find('.product-item__middle-top'),
                    compare = $this.find('.product-item__compare'),
                    first = $this.find('.product-item__first');

                    code.appendTo(first)
                    compare.appendTo(first)
            })

            $('.product-list.thumbs')
                .removeClass('product-list--simple')
                .addClass('product-list--thumbs')

            thumbsListFlag = 1
        }   
    }
}

$(window).on('resize load', function(){
    priceList();
    simpleList();
    thumbsList();
})

// if ($('.product-list.thumbs').length) {
//     if($(window).width() <= 768) {
//         $('.product-list.thumbs ._js_product-item').each(function(){
//             var $this = $(this),
//                 code = $this.find('.product-item__code'),
//                 compare = $this.find('.product-item__compare'),
//                 last = $this.find('.product-item__last'),
//                 middleTop = $this.find('.product-item__middle-top');

//                 compare.prependTo(last)
//                 code.prependTo(middleTop)
//         });    
//         $('.product-list.thumbs')
//             .removeClass('product-list--thumbs')
//             .addClass('product-list--simple')
//     } else {
//         $('.product-list.thumbs ._js_product-item').each(function(){
//             var $this = $(this),
//                 code = $this.find('.product-item__code'),
//                 compare = $this.find('.product-item__compare'),
//                 first = $this.find('.product-item__first'),
//                 middleTop = $this.find('.product-item__middle-top');

//                 compare.prependTo(first)
//                 code.prependTo(first)
//         }); 
//         $('.product-list.thumbs')
//             .removeClass('product-list--simple')
//             .addClass('product-list--thumbs')
//     }
// }

// if ($('.product-list--simple.simple').length) {
//     if($(window).width() <= 768) {
//         $('.product-list.simple ._js_product-item').each(function(){
//             var $this = $(this),
//                 compare = $this.find('.product-item__compare'),
//                 middleTop = $this.find('.product-item__middle-top'),
//                 last = $this.find('.product-item__last'),
//                 code = $this.find('.product-item__code');
//                 code.prependTo(middleTop)
//                 compare.appendTo(last)
//         });    
//     } else {
//         $('.product-list.simple ._js_product-item').each(function(){
//             var $this = $(this),
//                 middleTop = $this.find('.product-item__middle-top'),
//                 code = $this.find('.product-item__code'),
//                 compare = $this.find('.product-item__compare'),
//                 last = $this.find('.product-item__last');
//                 compare.appendTo(last)
//                 code.prependTo(middleTop)
//         });    
//     }
// }