$('.lawyer-fields-handy-btn').on('click', function(event) {
  event.preventDefault();

  $(this).toggleClass('is-opened');

  $('.lawyer-fields-handy').slideToggle();
});



let getProductMethosRadios = $('.new-cart-form-radio-button-group [type="radio"]');
let courierFields = $('.courier-fields');

getProductMethosRadios.on('change', function() {

  if (getProductMethosRadios.filter(":checked").val() == 2) {
    courierFields.stop().slideDown();
  } else {
    courierFields.stop().slideUp();
  }
});
