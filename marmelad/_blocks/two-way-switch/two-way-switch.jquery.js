let cartonMapCoords = $('.cart-on-map__coords');
let cartonMapList = $('.cart-on-map__list');

$('.cart-on-map .two-way-switch').on('click', function() {

  if ($(this).find('[type="checkbox"]')[0].checked) {
    cartonMapCoords.stop().slideUp();
    cartonMapList.stop().slideDown();
    console.log('checked');
  } else {
    cartonMapCoords.stop().slideDown();
    cartonMapList.stop().slideUp();
  }

});
