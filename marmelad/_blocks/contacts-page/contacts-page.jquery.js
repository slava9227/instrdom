if ($('#contacts-page-map').length) {
    var myMap, 
    	route,
    	ch =1,
    	markers = [],
    	point = [];

    var myAddress = 'г. Москва, ул. Электродная, д.2, стр.7',
    	centerMap = $('#contacts-page-map').data('center').split(',');

    function init () {
        // Поиск координат
        ymaps.geocode(myAddress, { results: 1 }).then(function (res) {
            // Выбираем первый результат геокодирования
            var firstGeoObject = res.geoObjects.get(0);
            var cords = firstGeoObject.geometry.getCoordinates();
            
            centerMap = cords;

        }, function (err) {
            // Если геокодирование не удалось,
            // сообщаем об ошибке
            alert(err.message);
        });

        myMap = new ymaps.Map('contacts-page-map', {
                center: centerMap, 
                zoom: 14
            });

    	//Добавляем элементы управления	
    	 myMap.controls                
            .add('zoomControl')               
            .add('typeSelector')
            .add('mapTools');

        var myPlacemark = new ymaps.Placemark(centerMap, {
            hintContent: myAddress,
            balloonContent: myAddress
        }, {
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: '../img/logomap.svg',
            // Размеры метки.
            iconImageSize: [128, 54],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-120, -54]
        });

            
     	markers.push(myPlacemark);
        myMap.geoObjects.add(myPlacemark);


    	//Отсеживаем событие клика по карте		
    	myMap.events.add('click', function (e) {                
    	    var coords = e.get('coordPosition');
    		if(markers.length < 10) {
    			if (markers.length >= 1) {
    				reset();

                    var myPlacemark = new ymaps.Placemark(centerMap, {
                        hintContent: myAddress,
                        balloonContent: myAddress
                    }, {
                        // Опции.
                        // Необходимо указать данный тип макета.
                        iconLayout: 'default#image',
                        // Своё изображение иконки метки.
                        iconImageHref: '../img/logomap.svg',
                        // Размеры метки.
                        iconImageSize: [128, 54],
                        // Смещение левого верхнего угла иконки относительно
                        // её "ножки" (точки привязки).
                        iconImageOffset: [-120, -54]
                    });
                    markers.push(myPlacemark);

                    myMap.geoObjects.add(myPlacemark);

                    ch = 2;

        			myPlacemark = new ymaps.Placemark([coords[0].toPrecision(6),coords[1].toPrecision(6)], {
        	            // Свойства
        	            // Текст метки
        	            iconContent: ch
        	        }, {
        	            // Опции
        	            // Иконка метки будет растягиваться под ее контент
        	            preset: 'twirl#blueStretchyIcon'
        	        });

        		 	markers.push(myPlacemark);
        			myMap.geoObjects.add(myPlacemark);
        		 	ch++;
        		 	calcRoute()
                }
    		 } else {
    		 	alert("Вы задали максимальное количество точек");
    		 }
    	});	
    }

    function calcRoute() {		
    	for(var i = 0, l = markers.length; i < l; i++) {
    		point[i] = markers[i].geometry.getCoordinates();
    	}

        ymaps.route(point, {
            // Опции маршрутизатора
            mapStateAutoApply: true // автоматически позиционировать карту
        }).then(function (router) {

    		route = router;
            myMap.geoObjects.add(route);
        }, function (error) {
            alert("Возникла ошибка: " + error.message);
        });		
    }

    //Удаление маршрута и меток с карты и очистка данных
    function reset() {
    	route && myMap.geoObjects.remove(route);
    	for(var i = 0, l = markers.length; i < l; i++) {
    		myMap.geoObjects.remove(markers[i]);
    	}
    	markers = []; 
    	point = [];
    	ch = 1;
    }

    ymaps.ready(init);
}