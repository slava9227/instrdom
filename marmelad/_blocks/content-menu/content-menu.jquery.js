$('.content-menu > ul > li').each(function(){
	var $this = $(this),
		subLevels = $this.find('>ul');		

	if($this.children('ul').length) {
		$this.find('>a').clone().addClass('back').prependTo(subLevels).wrap('<li>').find('.icon').remove()
	}
	
});

$('.content-menu > ul > li > a').on('click tap', function(event){
	var subLevels = $(this).find('>ul');
	if($(this).parent().children('ul').length) {
		event.preventDefault();
		documentAddClassOverflowHidden();
		$(this).parent().addClass('opened')
	}
})

$('.content-menu ul li .back').on('click tap', function(event){
	event.preventDefault();
	$(this).closest('.has-levels').removeClass('opened')
	documentRemoveClassOverflowHidden();
})
