// function validateEmail($email) {
//  	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
//  	return emailReg.test( $email );
// }

$('._js_selected_country').on('click tap', function(){	
	$(this).parent().toggleClass('opened')
});

$('._js_select_country div').on('click tap', function(){
	var $this = $(this),
		phoneNumber = $this.data('phone-number'),
		placeholder = phoneNumber.slice(0,2);

		$this
			.closest('.b-form__item--phone')
			.find('._js_phone-number')
			.val(placeholder)
			// .mask(phoneNumber);

	$('.b-form__country-flag').removeClass('opened')

});

$('[type=tel]').bind("change keyup input click", function() {
	if (this.value.match(/[^0-9,+]/g)) {
		this.value = this.value.replace(/[^0-9,+]/g, '');
	}
});

// $('input[type=tel]').mask("+7 (999) 99 99 99");
// $('input[type=tel]').inputmask('(99) 9999[9]-9999');

$('.b-form__item-close').on('click tap', function(){
	$(this).closest('.b-form__item').removeClass('error')
});

$('input[type="email"]').focusout(function(){
	if($(this).closest('.b-form').find('input[type="email"]:invalid').length) {
		$(this).closest('.b-form').find('input[type="email"]').closest('.b-form__item').addClass('error');	
	}
})

$('.b-form .b-form__button').on('click', function(event){	

	if ($(this).closest('.b-form').find('input[type="email"]:invalid').length) {
		$(this).closest('.b-form').find('input[type="email"]').closest('.b-form__item').addClass('error');
	} else {
		$(this).closest('.b-form').find('input[type="email"]').closest('.b-form__item').removeClass('error');
	}

	if($(this).hasClass('disabled') || $(this).closest('.b-form').find('input[type="tel"]').val().length < 12 || $(this).closest('.b-form').find('input[type="email"]').val() == "") {
		return false
	}

});
