var inst1 = $('[data-remodal-id=modal1]').remodal();
var inst2 = $('[data-remodal-id=modal2]').remodal();

$('.remodal-close').append('<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><rect x="22.9336" y="0.157715" width="1.28827" height="32.2067" transform="rotate(45 22.9336 0.157715)" fill="#3C4858"/><rect x="0.160156" y="1.06885" width="1.28827" height="32.2067" transform="rotate(-45 0.160156 1.06885)" fill="#3C4858"/></svg>')

$(document).on('click tap', '.remodal-close', function(){
	return false
	// $(window).scroll(function() { return false; })
})

$('.form.application').parent('.remodal-overlay').addClass('application-wrapper')


$(window).on('resize load', function(){
	$('.form.application .b-form ').css({
		'height': $(window).height()
	})	
})

$('input[type="checkbox"]').styler()

$('.type-1.has-file input[type="file"]').styler({
	filePlaceholder: 'Прикрепить реквизиты',
	fileBrowse: "Обзор..."
});

var file_onchange = function () {
	var extension = $(this).val().split('.').pop().toLowerCase();
	var maxSize = 10485760;

	if($.inArray(extension, ['doc','docx','xls','xlsx','pdf']) == -1) {
		$(this).closest('.has-file').addClass('error expansion').removeClass('size')
	} else if (this.files[0].size > 10485760) {
		$(this).closest('.has-file').addClass('error size').removeClass('expansion')	
	    console.log(2)		
	} else {
	    console.log(3)		
	}
};

$('.b-form input[type="file"]').on('change', file_onchange);


$(document).on('click tap', '.b-form__item--name .jq-file__refresh', function(){
	$(this).closest('.b-form__item--name').removeClass('error')
	$('.b-form__item--name input[type="file"]').trigger('refresh');
	$('.b-form__item--name .jq-file__name').text('Прикрепить реквизиты')	
	$('.b-form__item--name .jq-file').append('<div class="jq-file__refresh">удалить</div>')
})

$('.b-form__item.b-form__item--file input[type="file"]').styler({
	filePlaceholder: 'Просто перетащите сюда файл с заявкой из вашего компьютера или',
	fileBrowse: "Прикрепить файл"
});


$('.type-2.has-file input[type="file"]').styler({
	filePlaceholder: 'Прикрепить вашу ЗАЯВКУ',
	fileBrowse: "Прикрепить файл"
});

$(document).on('click tap', '.b-form__item--file .jq-file__refresh', function() {
	$(this).closest('.b-form__item--file').removeClass('error')
	$('.b-form__item--file input[type="file"]').trigger('refresh');
	$('.b-form__item--file .jq-file__name').text('Просто перетащите сюда файл с заявкой из вашего компьютера или');
	$('.b-form__item--file .jq-file').append('<div class="jq-file__refresh">удалить</div>');
})

$(document).on('click tap', '.type-1.has-file .jq-file__refresh', function() {
	$(this).closest('.has-file').removeClass('error')
	$('.type-1.has-file input[type="file"]').trigger('refresh');
	$('.type-1.has-file .jq-file__name').text('Прикрепить реквизиты')
	$('.type-1.has-file .jq-file').append('<div class="jq-file__refresh">удалить</div>')
})

$(document).on('click tap', '.type-2.has-file .jq-file__refresh', function() {
	$(this).closest('.has-file').removeClass('error')
	$('.type-2.has-file input[type="file"]').trigger('refresh');
	$('.type-2.has-file .jq-file__name').text('Прикрепить вашу ЗАЯВКУ')
	$('.type-2.has-file .jq-file').append('<div class="jq-file__refresh">удалить</div>')
})

$('.has-file .jq-file').append('<div class="jq-file__refresh">удалить</div>')

$('.b-form__item--phone input').on('keyup', function(){
	var $this = $(this),
		inputValue = $this.val();
	if(inputValue.length >= 12) {	
		$this.addClass('true')	
		$this.closest('.b-form').find('.b-form__item--phone').addClass('valid')
	} else {
		$this.removeClass('true')	
		$this.closest('.b-form').find('.b-form__item--phone').removeClass('valid')		
	}
});

function validateForm() {
	var item = $('.application .b-form .b-form__item'),
		inputName = $('.application .b-form .inputName').val(),
		inputEmail = $('.application .b-form .inputEmail').val(),
		inputPhone = $('.application .b-form .b-form__item--phone input'),
		inputAgreemet = $('.application .b-form .b-form__item--greement input');


	if(inputName != "" && inputEmail != "" && inputPhone.hasClass('true') && inputAgreemet.is(':checked') == true && $('.application .b-form .b-form__item.error').length == 0) {
		$('.application .b-form .b-form__button').removeClass('disabled')
	} else {
		$('.application .b-form .b-form__button').addClass('disabled')		
	}
}

setInterval(function(){
	validateForm()
}, 200)