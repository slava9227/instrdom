$('.b-categor-filter__item').on('click', function(e){
	e.preventDefault();

	var filterId = $(this).data('filter');

	$('.b-categor-block__li').removeClass('hide');
	if (filterId != 'all') {
		$('.b-categor-block__ul').each(function(){
			var $thisUl = $(this),
				filteLi = $thisUl.find('.b-categor-block__li[data-categ="'+ filterId +'"]');

			$thisUl.find('.b-categor-block__li').not(filteLi).addClass('hide').appendTo($thisUl);
		});
	}

	$('.b-categor-filter__item').removeClass('active');
	$(this).addClass('active');
});