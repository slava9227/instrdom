function validateEmail(email)  {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  return re.test(String(email).toLowerCase());
}

$('.floated-input__input').on('input', function() {
  let $self = $(this);
  let $parent = $self.closest('.floated-input');
  let $validator = $self.attr('type');

  if ($self.val().length > 0) {
    $parent.addClass('is-filled');

    if ($validator == 'email') {
      if (validateEmail($self.val())) {
        $parent
          .removeClass('is-error')
          .addClass('is-success');
      } else {
        $parent
          .removeClass('is-success')
          .addClass('is-error');
      }
    }

  } else {
    $parent.removeClass('is-filled is-error is-valid');
  }
});
