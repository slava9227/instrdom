if($('.landing-header--fixed').length) {
	var maxHeight = 0;
	$(window).on('scroll', (function(){
		var old_scroll = $(window).scrollTop();
		if (old_scroll > maxHeight) {
		  $(".landing-header--fixed").addClass('scroller');
		}
		else {
		  $(".landing-header--fixed").removeClass('scroller');
		}
	}));
}

if($('.landing-header__menu').length) {
	$('.landing-header__menu').appendTo('.mobile-panel__content').removeClass('landing-header__menu--hidden');
}