let $cartTable = $('.cart-table');
let $cartTableWrap = $cartTable.closest('.cart-order__table-wrap');
let $cartTableRows = $cartTable.find('.cart-table__body-row');
let $slideCartTable = $('.cart-table-slide');

function getFirstThreeRowsHeight(rows) {
  return rows.eq(0).outerHeight(true) + rows.eq(1).outerHeight(true) + rows.eq(2).outerHeight(true)
}

function setCartTableHeight() {
  console.log(getFirstThreeRowsHeight($cartTableRows));
  $cartTableWrap.css({
    height: getFirstThreeRowsHeight($cartTableRows)
  });
}

if ($cartTableRows.length > 3) {
  setCartTableHeight();
  $(window).on('resize', setCartTableHeight);
}

$slideCartTable.on('click', function(event) {
  event.preventDefault();

  if ($(this).hasClass('is-opened')) {
    $(this).removeClass('is-opened');

    $cartTableWrap.animate({
      height: getFirstThreeRowsHeight($cartTableRows)
    }, { queue:false, duration:500 });
  } else {
    $(this).addClass('is-opened');

    $cartTableWrap.animate({
      height: $cartTable.outerHeight(true)
    }, { queue:false, duration:300 });
  }
});
