module.exports = {
  app: {
    lang: 'ru',
    stylus: {
      theme_color: '#21333B',
    },
    GA: false, // Google Analytics site's ID
    package: 'ключ перезаписывается значениями из package.json marmelad-сборщика',
    settings: 'ключ перезаписывается значениями из файла настроек settings.marmelad.js',
    storage: 'ключ перезаписывается значениями из файла настроек settings.marmelad.js',
    buildTime: '',
    controls: [
      'default',
      'brand',
      'success',
      'info',
      'warning',
      'danger',
    ],
  },

    pageTitle: 'marmelad',

    menuTop : [
        {
            'name': "О компании"
        },
        {
            'name': "Магазин",
            'subLevelClass': true
        },
        {
            'name': "Самовывоз",
            'activeClass': true
        },
        {
            'name': "Доставка/Оплата"
        },
        {
            'name': "Контакты"
        },
        {
            'name': "Юридическим лицам",
            'bold': true
        }
    ],

    folderList: [
        {
            'icon': 'fl-ico1',
            'name': 'Аккумуляторные <br> дрели'
        },
        {
            'icon': 'fl-ico2',
            'name': 'Штангенциркули'
        },
        {
            'icon': 'fl-ico3',
            'name': 'Гаечные ключи'
        },
        {
            'icon': 'fl-ico4',
            'name': 'Отвертки'
        },
        {
            'icon': 'fl-ico5',
            'name': 'Линейки'
        },
        {
            'icon': 'fl-ico6',
            'name': 'Уровни'
        },
        {
            'icon': 'fl-ico7',
            'name': 'Молотки'
        },
        {
            'icon': 'fl-ico7',
            'name': 'Молотки'
        }
    ],
    categBlock: [
        {
            'categ': 'industry',
            'name': 'Штангенциркули',
            'pic': 'img/pr-list-th-02.jpg'
        },
        {
            'categ': 'construction',
            'name': 'Угольники',
            'pic': 'img/pr-img1.jpg'
        },
        {
            'categ': 'repair',
            'name': 'РУЛЕТКИ',
            'pic': 'img/pr-list-th-03.jpg'
        },
        {
            'categ': 'industry',
            'name': 'НИВЕЛИРЫ',
            'pic': 'img/no-image.png'
        },
        {
            'categ': 'repair',
            'name': 'Глубиномеры ',
            'pic': 'img/no-image.png'
        },
        {
            'categ': 'construction',
            'name': 'Угломеры и уклономеры',
            'pic': 'img/no-image.png'
        },
        {
            'categ': 'industry',
            'name': 'ЛАЗЕРНЫЕ ДАЛЬНОМЕРЫ',
            'pic': 'img/no-image.png'
        },
        {
            'categ': 'repair',
            'name': 'Глубиномеры ',
            'pic': 'img/no-image.png'
        },
        {
            'categ': 'construction',
            'name': 'УРОВНИ',
            'pic': 'img/no-image.png'
        }
    ],

    productList :[
        {
            'image': 'pr-img1.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '2 789',
            'priceOld': ''
        },   
        {
            'image': 'pr-list-th-02.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '4 692',
            'priceOld': '4 692',
            'minPurchase': true
        }, 
        {
            'image': 'pr-list-th-03.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '1 000',
            'priceOld': ''
        }, 
        {
            'image': 'pr-list-th-04.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '789',
            'priceOld': ''
        }, 
        {
            'image': 'pr-list-th-05.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '3 600',
            'priceOld': ''
        }, 
        {
            'image': 'pr-list-th-06.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '1 060',
            'priceOld': ''
        },
        {
            'image': 'pr-list-th-07.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '2 005',
            'priceOld': ''
        },
        {
            'image': 'pr-list-th-08.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '1 646',
            'priceOld': ''
        },
        {
            'image': 'pr-list-th-09.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '9 000',
            'priceOld': ''
        },
        {
            'image': 'pr-list-th.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '2 789',
            'priceOld': ''
        },   
        {
            'image': 'pr-list-th-02.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '4 692',
            'priceOld': '4 692'
        }, 
        {
            'image': 'pr-list-th-03.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '1 000',
            'priceOld': ''
        }, 
        {
            'image': 'pr-list-th-04.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '789',
            'priceOld': ''
        }, 
        {
            'image': 'pr-list-th-05.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '3 600',
            'priceOld': ''
        }, 
        {
            'image': 'pr-list-th-06.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '1 060',
            'priceOld': ''
        },
        {
            'image': 'pr-list-th-07.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '2 005',
            'priceOld': ''
        },
        {
            'image': 'pr-list-th-08.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '1 646',
            'priceOld': ''
        },
        {
            'image': 'pr-list-th-09.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '9 000',
            'priceOld': ''
        }
        
    ],

    recentlyViewed : [
        {
            'image': 'pr-list-th.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '2 789',
            'priceOld': ''
        },   
        {
            'image': 'pr-list-th-02.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '4 692',
            'priceOld': '4 692'
        }, 
        {
            'image': 'pr-list-th-03.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '1 000',
            'priceOld': ''
        }, 
    ],
    recentlyViewed2 : [
        {
            'image': 'pr-img1.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '2 789',
            'priceOld': ''
        },   
        {
            'image': 'pr-list-th.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '2 789',
            'priceOld': ''
        },   
        {
            'image': 'pr-list-th-02.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '4 692',
            'priceOld': '4 692'
        }, 
        {
            'image': 'pr-list-th-03.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '1 000',
            'priceOld': ''
        }, 
    ],
    params : [
        {
            'name': 'Производитель',
            'value': 'Энкор'
        }, 
        {
            'name': 'Страна',
            'value': 'Китай'
        }, 
        {
            'name': 'Тип',
            'value': 'ШЦ-1'
        }, 
        {
            'name': 'Диапазон измерения',
            'value': '0-300'
        },   
        {
            'name': 'Цена деления',
            'value': '0.02'
        }
    ],

    productMain :[
        {
            'image': 'product-img2.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '2 789',
            'priceOld': ''
        },   
        {
            'image': 'product-img3.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447 ADA Cube 2-360 Basic Edition A00447',
            'price': '2 789',
            'priceOld': ''
        }, 
        {
            'image': 'product-img4.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '2 789',
            'priceOld': ''
        }, 
        {
            'image': 'product-img5.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '2 789',
            'priceOld': ''
        }, 
        {
            'image': 'product-img6.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '2 789',
            'priceOld': ''
        }, 
        {
            'image': 'product-img7.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '2 789',
            'priceOld': ''
        },
        {
            'image': 'product-img8.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '2 789',
            'priceOld': ''
        },
        {
            'image': 'product-img9.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '2 789',
            'priceOld': ''
        },
        {
            'image': 'product-img10.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '2 789',
            'priceOld': ''
        }, 
        {
            'image': 'product-img11.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '2 789',
            'priceOld': ''
        }
    ],

    productMain2 :[
        {
            'image': 'product-img12.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '2 789',
            'priceOld': ''
        },   
        {
            'image': 'product-img13.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447 ADA Cube 2-360 Basic Edition A00447',
            'price': '2 789',
            'priceOld': ''
        }, 
        {
            'image': 'product-img14.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '2 789',
            'priceOld': ''
        }, 
        {
            'image': 'product-img15.jpg',
            'name': 'ADA Cube 2-360 Basic Edition A00447',
            'price': '2 789',
            'priceOld': ''
        }
    ],

    iconsBlock : [
        {
            'icon': "bl-icon1.svg",
            'text': "Специальные цены уже <br> при первом заказе",
        },
        {
            'icon': "bl-icon2.svg",
            'text': "Быстрая обработка <br> заказа",
        },
        {
            'icon': "bl-icon3.svg",
            'text': "Закрытие заявки <br> на 100%",
        },
        {
            'icon': "bl-icon4.svg",
            'text': "Более 10 000 наименований <br> инструментов",
        },
        {
            'icon': "bl-icon5.svg",
            'text': "Отсрочка платежа",
        },
        {
            'icon': "bl-icon6.svg",
            'text': "Личный менеджер",
        },
        {
            'icon': "bl-icon7.svg",
            'text': "Счет за 15 минут",
        },
        {
            'icon': "bl-icon8.svg",
            'text': "Доставка по всей России",
        }
    ],

    form : {
        name : "Название формы",
        mod : "",
        items: [
            {
                'name': "Имя",
                'type': 'text',
                'note': 'Это поле обязательно для заполнения',
                'mod': '',
                'placeholder': true,
                'required': true,
                'error': false,
            },
            {
                'name': "Сообщение",
                'type': 'textarea',
                'note': '',
                'mod': '',
                'placeholder': true,
                'required': false,
                'error': false,
            },
            {
                'name': "Выпадающий список",
                'type': 'select',
                'note': '',
                'mod': '',
                'placeholder': true,
                'required': false,
                'error': false,
                options : [
                    {
                        'name' : 'value 1'
                    },
                    {
                        'name' : 'value 2'
                    },
                    {
                        'name' : 'value 3'
                    },
                    {
                        'name' : 'value 4'
                    },
                ]
            },
            {
                'name': "Текстовый блок",
                'type': 'textBlock',               
                'text': 'Lorem ipsum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',    
                'placeholder': true,
            },
            {
                'name': "Радиобокс",
                'type': 'radio',
                'note': '',
                'mod': '',
                'placeholder': true,
                'required': false,
                'error': false,
            },
            {
                'name': "Имя",
                'type': 'checkbox',
                'note': '',
                'mod': '',
                'placeholder': true,
                'required': true,
                'error': false,
            }
        ]
    }
};
