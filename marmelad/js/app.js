/**
 * Подключение JS файлов которые начинаются с подчеркивания
 */
//=require ../_blocks/**/_*.js


/* ^^^
 * JQUERY Actions
 * ========================================================================== */
$(function() {

    'use strict';

    /**
     * определение существования элемента на странице
     */
    $.exists = (selector) => $(selector).length > 0;

    /**
     * [^_]*.js - выборка всех файлов, которые не начинаются с подчеркивания
     */
    //=require ../_blocks/**/[^_]*.jquery.js

    $('.document-scroll-icon span,.landing-banner__document-scroll-icon').on('click tap', function(){
    	$('html, body').animate({scrollTop: $(window).height()}, 800)    	
    })

    function menuHeight() {
        if($('.vertical.desctop').length) {
            $('.vertical.desctop > li > ul').css({
                'width': ($(window).width() - 335) - $('.horisontal').offset().left 
            })
        }
        
        var mh = 0;
        $('.app-header__folders-shared .vertical.desctop > li > ul').each(function () {
           var h_block = parseInt($(this).innerHeight());
           if(h_block > mh) {
              mh = h_block;
           };
        });

        $('.app-header__folders-shared .vertical.desctop > li > ul').height(mh - 16);
        $('.vertical.desctop').height(mh + 1);
    }

    var flag1023 = 1;
    var searcgResizeFlag = 1;
    var flag640 = 1;

    function siteResize() {
        if($('.content-menu').length) {
            if (window.matchMedia('(max-width:640px)').matches && searcgResizeFlag == 1) {
                $('._js_search').addClass('mobile main').prependTo('.content-menu')
                searcgResizeFlag = 2
            } else if (window.matchMedia('(min-width:641px)').matches && searcgResizeFlag == 2) {
                $('._js_search').removeClass('mobile main').appendTo('.app-header__middle')
                searcgResizeFlag = 1
            }
        } else {
            if (window.matchMedia('(max-width:1023px)').matches && searcgResizeFlag == 1) {
                $('._js_search').addClass('mobile catalog').appendTo('.c-t-container')
                searcgResizeFlag = 2
            } else if (window.matchMedia('(min-width:1024px)').matches && searcgResizeFlag == 2) {
                $('._js_search').removeClass('mobile catalog').appendTo('.app-header__middle')
                searcgResizeFlag = 1
            }
        }

        if (window.matchMedia('(max-width:1023px)').matches && flag1023 == 1) {         
            $('.app-header__cart').prependTo('.mobile-panel__content')
            $('.app-header__menu').appendTo('.mobile-panel__content')
            $('.app-header__folders-shared').appendTo('.mobile-panel__content')
            $('.sort-by').addClass('mobile').appendTo('.filter-sorting-wrapper') 
            // $('.b-filter').appendTo('.filter-sorting-wrapper__filter-container')   
            $('.b-filter').appendTo('body')   
            flag1023 = 2;
        } else if (window.matchMedia('(min-width:1024px)').matches && flag1023 == 2) {
            $('.app-header__mobile-container').after($('.app-header__menu'))
            $('.app-header__cart').appendTo('.app-header__middle')
            $('.app-header__folders-shared').appendTo('.app-header__bot')   
            $('.sort-by').removeClass('mobile').prependTo('.b-sorting__container') 
            $('.b-filter').appendTo('.app__aside--right')   
            flag1023 = 1;
        }

        menuHeight();

        // if (window.matchMedia('(max-width:640px)').matches && flag640 == 1) {         
        //     $('.b-filter').appendTo('body')   

        //     flag640 = 2
        // } else if (window.matchMedia('(min-width:641px)').matches && flag640 == 2) {            
        //     $('.b-filter').appendTo('.filter-sorting-wrapper__filter-container')   

        //     flag640 = 1
        // }

    }
    // setTimeout(function(){
    //     $('html, body').animate({scrollTop: $('.b-breadcrumbs').offset().top}, 500)
    // }, 20)

    $(window).on('resize', siteResize).trigger('resize');

});
